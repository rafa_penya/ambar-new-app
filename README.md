# Ambar Abogados App (Flutter)

Clone project from: https://bitbucket.org/syntonize/ambar_flutter_app/src/master/  
Staging URI: https://staging-2.ambarpartners.net/api/docs/ 

## Getting Started

1 - Create a file into /lib/global/ path named .env_dev  
2 - Insert into this variable ``API_URL={the staging url}`` (ensure this url is not on 500 or 502 code state)

Open project path and VC terminal, and execute:

``flutter doctor``

Open Iphone Simulator or Android Studio Device and run visual code debugger trying choice "DART PROJECT"