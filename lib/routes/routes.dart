import 'package:ambar_abogados/src/pages/discounts/discount_detail.dart';
import 'package:ambar_abogados/src/pages/discounts/discounts_screen.dart';
import 'package:ambar_abogados/src/pages/events/events.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/pages/products/products.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:flutter/material.dart';

import '../src/pages/auth/auth.dart';
import '../src/pages/drawer/agenda/agenda_events.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'login': (final context) => LoginScreen(),
  'passrecovery': (final context) => const PassRecoveryScreen(),
  'register': (final context) => const RegisterScreen(),
  'checking': (final context) => CheckAuthScreen(),
  'home': (final context) => const TabsPage(),
  'agenda': (final context) => const AgendaEvents(),
  'editprofile': (final context) => const EditProfile(),
  'changepassword': (final context) => const ChangePassword(),
  'addinterests': (final context) => const AddInterests(),
  'notifications': (final context) => const NotificationsScreen(),
  'availability': (final context) => AvailabilityScreen(),
  'referral': (final context) => const ReferralScreen(),
  'awards': (final context) => const AwardsScreen(),
  'servicesproducts': (final context) => ServicesProductsScreen(),
  'events': (final context) => EventsScreen(),
  'eventdetail': (final context) => EventDetailScreen(),
  'proserv': (final context) => ServicesProductsScreen(),
  'proservdetail': (final context) => ProductServiceDetailScreen(),
  'discounts': (final context) => DiscountsScreen(),
  'discountdetail': (final context) => DiscountDetailScreen(),
  'select': (final context) => SelectDrop(),
};
