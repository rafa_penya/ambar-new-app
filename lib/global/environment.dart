import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';

class Environment {
  static String apiUrl =
      Platform.isAndroid ? dotenv.get('API_URL') : dotenv.get('API_URL');
}
