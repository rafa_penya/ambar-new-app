import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class DioApi {
  static Dio _dio = new Dio();

  static Future<void> configureDio() async {
    final storage = FlutterSecureStorage();
    final basicAuth = await storage.read(key: 'authbasic');
    //base url
    _dio.options.baseUrl = dotenv.get('API_URL');

    //headers

    _dio.options.headers = {
      'Content-type': 'application/json; charset=utf-8',
      'Accept': 'application/json',
      "Authorization": 'Basic ${basicAuth}'
    };
  }

  static Future httpGet(String path, {required Options options}) async {
    try {
      final response = await _dio.get(path);

      return response.data;
    } catch (e) {
      print(e);
      throw ('Error en el GET');
    }
  }

  static Future httpPost(
    String path,
    data,
  ) async {
    final formData = FormData.fromMap(data);
    try {
      final response = await _dio.post(path, data: formData);
      return response.data;
    } catch (e) {
      print(e);
      throw ('Error en el POST');
    }
  }
}
