// To parse this JSON data, do
//
//     final events = eventsFromJson(jsonString);

import 'dart:convert';

class Events {
  Events({
    this.count,
    this.next,
    this.previous,
    this.results,
    this.pageSize,
  });

  int? count;
  dynamic next;
  dynamic previous;
  List<ResultEvents>? results;
  int? pageSize;

  factory Events.fromRawJson(String str) => Events.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Events.fromJson(Map<String, dynamic> json) => Events(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results: json["results"] == null
            ? []
            : List<ResultEvents>.from(
                json["results"]!.map((x) => ResultEvents.fromJson(x))),
        pageSize: json["page_size"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": results == null
            ? []
            : List<dynamic>.from(results!.map((x) => x.toJson())),
        "page_size": pageSize,
      };
}

class ResultEvents {
  ResultEvents({
    required this.id,
    this.homePosition,
    required this.universe,
    required this.image,
    required this.title,
    required this.description,
    this.places,
    this.placeType,
    required this.startDate,
    this.endDate,
    required this.isOrganizedByAmbar,
    this.attachment,
    this.dateDescription,
    this.productType,
    this.addressName,
    this.address,
    this.addressLink,
    this.onlineLink,
    this.maxPartnerPlaces,
    this.products,
    this.currentPurchasedPlaces,
  });

  int id;
  int? homePosition;
  String universe;
  String image;
  String title;
  String description;
  int? places;
  String? placeType;
  DateTime startDate;
  DateTime? endDate;
  bool isOrganizedByAmbar;
  String? attachment;
  String? dateDescription;
  String? productType;
  String? addressName;
  String? address;
  String? addressLink;
  String? onlineLink;
  int? maxPartnerPlaces;
  List<Product>? products;
  int? currentPurchasedPlaces;

  factory ResultEvents.fromRawJson(String str) =>
      ResultEvents.fromJson(json.decode(str));

  get productData => null;

  String toRawJson() => json.encode(toJson());

  factory ResultEvents.fromJson(Map<String, dynamic> json) => ResultEvents(
        id: json["id"],
        homePosition: json["home_position"],
        universe: json["universe"],
        image: json["image"],
        title: json["title"],
        description: json["description"],
        places: json["places"],
        placeType: json["place_type"],
        startDate: json["start_date"] = DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        isOrganizedByAmbar: json["is_organized_by_ambar"] == null,
        attachment: json["attachment"],
        dateDescription: json["date_description"] = json["date_description"],
        productType: json["product_type"],
        addressName: json["address_name"],
        address: json["address"],
        addressLink: json["address_link"],
        onlineLink: json["online_link"],
        maxPartnerPlaces: json["max_partner_places"],
        products: json["products"] == null
            ? []
            : List<Product>.from(
                json["products"]!.map((x) => Product.fromJson(x))),
        currentPurchasedPlaces: json["current_purchased_places"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "home_position": homePosition,
        "universe": universe,
        "image": image,
        "title": title,
        "description": description,
        "places": places,
        "place_type": placeType,
        "start_date": startDate.toIso8601String(),
        "end_date": endDate?.toIso8601String(),
        "is_organized_by_ambar": isOrganizedByAmbar,
        "attachment": attachment,
        "date_description": dateDescription,
        "product_type": productType,
        "address_name": addressName,
        "address": address,
        "address_link": addressLink,
        "online_link": onlineLink,
        "max_partner_places": maxPartnerPlaces,
        "products": products == null
            ? []
            : List<dynamic>.from(products!.map((x) => x.toJson())),
        "current_purchased_places": currentPurchasedPlaces,
      };
}

class Product {
  Product({
    this.id,
    this.name,
    this.description,
    this.price,
    this.partnerProducts,
  });

  int? id;
  String? name;
  String? description;
  String? price;
  List<PartnerProduct>? partnerProducts;

  factory Product.fromRawJson(String str) => Product.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        price: json["price"],
        partnerProducts: json["partner_products"] == null
            ? []
            : List<PartnerProduct>.from(json["partner_products"]!
                .map((x) => PartnerProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "price": price,
        "partner_products": partnerProducts == null
            ? []
            : List<dynamic>.from(partnerProducts!.map((x) => x.toJson())),
      };
}

class PartnerProduct {
  PartnerProduct({
    this.id,
    this.places,
    this.price,
  });

  int? id;
  int? places;
  String? price;

  factory PartnerProduct.fromRawJson(String str) =>
      PartnerProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PartnerProduct.fromJson(Map<String, dynamic> json) => PartnerProduct(
        id: json["id"],
        places: json["places"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "places": places,
        "price": price,
      };
}
