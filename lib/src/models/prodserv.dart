// To parse this JSON data, do
//
//     final productSer = productSerFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class ProductSer {
  ProductSer({
    required this.count,
    required this.next,
    required this.previous,
    required this.results,
    required this.pageSize,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<ResultProdSer> results;
  int pageSize;

  factory ProductSer.fromRawJson(String str) =>
      ProductSer.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductSer.fromJson(Map<String, dynamic> json) => ProductSer(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results: List<ResultProdSer>.from(
            json["results"].map((x) => ResultProdSer.fromJson(x))),
        pageSize: json["page_size"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
        "page_size": pageSize,
      };
}

class ResultProdSer {
  ResultProdSer({
    required this.id,
    this.homePosition,
    required this.universe,
    required this.image,
    required this.title,
    required this.description,
    required this.attachment,
    required this.isOrganizedByAmbar,
    this.maxPartnerPurchases,
    this.productType,
    this.currentSubProductPurchases,
    this.productSubProducts,
  });

  int id;
  int? homePosition;
  Universe universe;
  String image;
  String title;
  String description;
  dynamic attachment;
  bool isOrganizedByAmbar;
  int? maxPartnerPurchases;
  String? productType;
  int? currentSubProductPurchases;
  List<ProductServPurchase>? productSubProducts;

  factory ResultProdSer.fromRawJson(String str) =>
      ResultProdSer.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ResultProdSer.fromJson(Map<String, dynamic> json) => ResultProdSer(
        id: json["id"],
        homePosition: json["home_position"],
        universe: universeValues.map[json["universe"]]!,
        image: json["image"],
        title: json["title"],
        description: json["description"],
        attachment: json["attachment"],
        isOrganizedByAmbar: json["is_organized_by_ambar"] == null,
        maxPartnerPurchases: json["max_partner_purchases"],
        productType: json["product_type"],
        currentSubProductPurchases: json["current_sub_product_purchases"],
        // productSubProducts:
        //     List<dynamic>.from(json["product_sub_products"].map((x) => x)),

        productSubProducts: json["product_sub_products"] == null
            ? []
            : List<ProductServPurchase>.from(json["product_sub_products"]!
                .map((x) => ProductServPurchase.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "home_position": homePosition,
        "universe": universeValues.reverse[universe],
        "image": image,
        "title": title,
        "description": description,
        "attachment": attachment,
        "is_organized_by_ambar": isOrganizedByAmbar,
        "max_partner_purchases": maxPartnerPurchases,
        "product_type": productType,
        "current_sub_product_purchases": currentSubProductPurchases,
        "product_sub_products": productSubProducts == null
            ? []
            : List<dynamic>.from(productSubProducts!.map((x) => x.toJson())),
      };
}

class ProductServPurchase {
  ProductServPurchase({
    this.subProduct,
    this.purchases,
    this.checkoutId,
    this.stripePublicKey,
  });

  int? subProduct;
  List<dynamic>? purchases;
  String? checkoutId;
  String? stripePublicKey;

  factory ProductServPurchase.fromRawJson(String str) =>
      ProductServPurchase.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductServPurchase.fromJson(Map<String, dynamic> json) =>
      ProductServPurchase(
        subProduct: json["sub_product"],
        purchases: json["purchases"],
        // == null
        //     ? []
        //     : List<dynamic>.from(
        //         json["purchases"]!.map((x) => dynamic.fromJson(x))),
        checkoutId: json["checkout_id"],
        stripePublicKey: json["stripe_public_key"],
      );

  Map<String, dynamic> toJson() => {
        "sub_product": subProduct,
        "purchases": purchases == null
            ? []
            : List<dynamic>.from(purchases!.map((x) => x.toJson())),
        "checkout_id": checkoutId,
        "stripe_public_key": stripePublicKey,
      };
}

enum Universe { GROWTH, SOCIAL, WELLBEING }

final universeValues = EnumValues({
  "growth": Universe.GROWTH,
  "social": Universe.SOCIAL,
  "wellbeing": Universe.WELLBEING
});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
