// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

class Profile {
  Profile({
    required this.firstName,
    required this.lastName,
    required this.email,
    this.phone,
    this.street,
    this.city,
    this.state,
    this.province,
    this.country,
    required this.zipCode,
    required this.dateOfBirth,
    this.practiceSubareas,
    this.subsectors,
    this.languages,
    this.interests,
    this.relevantOperations,
    this.workExperience,
    this.availability,
    this.startAbsence,
    this.endAbsence,
    this.profilePicture,
  });

  String firstName;
  String lastName;
  String email;
  String? phone;
  String? street;
  String? city;
  String? state;
  String? province;
  String? country;
  String zipCode;
  DateTime dateOfBirth;
  List<PracticeSubareaElement>? practiceSubareas;
  List<Subsector>? subsectors;
  List<LanguageElement>? languages;
  List<dynamic>? interests;
  List<dynamic>? relevantOperations;
  List<dynamic>? workExperience;
  String? availability;
  dynamic startAbsence;
  dynamic endAbsence;

  dynamic profilePicture;

  factory Profile.fromRawJson(String str) => Profile.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        phone: json["phone"],
        street: json["street"],
        city: json["city"],
        state: json["state"],
        province: json["province"],
        country: json["country"],
        zipCode: json["zip_code"],
        dateOfBirth: DateTime.parse(json["date_of_birth"]),
        practiceSubareas: json["practice_subareas"] == null
            ? []
            : List<PracticeSubareaElement>.from(json["practice_subareas"]!
                .map((x) => PracticeSubareaElement.fromJson(x))),
        subsectors: json["subsectors"] == null
            ? []
            : List<Subsector>.from(
                json["subsectors"]!.map((x) => Subsector.fromJson(x))),
        languages: json["languages"] == null
            ? []
            : List<LanguageElement>.from(
                json["languages"]!.map((x) => LanguageElement.fromJson(x))),
        interests: json["interests"] == null
            ? []
            : List<dynamic>.from(json["interests"]!.map((x) => x)),
        relevantOperations: json["relevant_operations"] == null
            ? []
            : List<dynamic>.from(json["relevant_operations"]!.map((x) => x)),
        workExperience: json["work_experience"] == null
            ? []
            : List<dynamic>.from(json["work_experience"]!.map((x) => x)),
        availability: json["availability"],
        startAbsence: json["start_absence"],
        endAbsence: json["end_absence"],
        profilePicture: json["profile_picture"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "phone": phone,
        "street": street,
        "city": city,
        "state": state,
        "province": province,
        "country": country,
        "zip_code": zipCode,
        "date_of_birth":
            "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth!.month.toString().padLeft(2, '0')}-${dateOfBirth!.day.toString().padLeft(2, '0')}",
        "practice_subareas": practiceSubareas == null
            ? []
            : List<dynamic>.from(practiceSubareas!.map((x) => x.toJson())),
        "subsectors": subsectors == null
            ? []
            : List<dynamic>.from(subsectors!.map((x) => x.toJson())),
        "languages": languages == null
            ? []
            : List<dynamic>.from(languages!.map((x) => x.toJson())),
        "interests": interests == null
            ? []
            : List<dynamic>.from(interests!.map((x) => x)),
        "relevant_operations": relevantOperations == null
            ? []
            : List<dynamic>.from(relevantOperations!.map((x) => x)),
        "work_experience": workExperience == null
            ? []
            : List<dynamic>.from(workExperience!.map((x) => x)),
        "availability": availability,
        "start_absence": startAbsence,
        "end_absence": endAbsence,
        "profile_picture": profilePicture,
      };
}

class LanguageElement {
  LanguageElement({
    this.id,
    this.level,
    this.language,
  });

  int? id;
  Level? level;
  LanguageLanguage? language;

  factory LanguageElement.fromRawJson(String str) =>
      LanguageElement.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LanguageElement.fromJson(Map<String, dynamic> json) =>
      LanguageElement(
        id: json["id"],
        level: json["level"] == null ? null : Level.fromJson(json["level"]),
        language: json["language"] == null
            ? null
            : LanguageLanguage.fromJson(json["language"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "level": level?.toJson(),
        "language": language?.toJson(),
      };
}

class LanguageLanguage {
  LanguageLanguage({
    this.id,
    this.name,
    this.getNameDisplay,
  });

  int? id;
  String? name;
  String? getNameDisplay;

  factory LanguageLanguage.fromRawJson(String str) =>
      LanguageLanguage.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LanguageLanguage.fromJson(Map<String, dynamic> json) =>
      LanguageLanguage(
        id: json["id"],
        name: json["name"],
        getNameDisplay: json["get_name_display"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "get_name_display": getNameDisplay,
      };
}

class Level {
  Level({
    this.id,
    this.displayValue,
  });

  int? id;
  String? displayValue;

  factory Level.fromRawJson(String str) => Level.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Level.fromJson(Map<String, dynamic> json) => Level(
        id: json["id"],
        displayValue: json["display_value"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "display_value": displayValue,
      };
}

class PracticeSubareaElement {
  PracticeSubareaElement({
    this.id,
    this.practiceSubarea,
    this.getLevelDisplay,
    this.level,
    this.subareaType,
  });

  int? id;
  PracticeSubareaPracticeSubarea? practiceSubarea;
  String? getLevelDisplay;
  String? level;
  String? subareaType;

  factory PracticeSubareaElement.fromRawJson(String str) =>
      PracticeSubareaElement.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PracticeSubareaElement.fromJson(Map<String, dynamic> json) =>
      PracticeSubareaElement(
        id: json["id"],
        practiceSubarea: json["practice_subarea"] == null
            ? null
            : PracticeSubareaPracticeSubarea.fromJson(json["practice_subarea"]),
        getLevelDisplay: json["get_level_display"],
        level: json["level"],
        subareaType: json["subarea_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "practice_subarea": practiceSubarea?.toJson(),
        "get_level_display": getLevelDisplay,
        "level": level,
        "subarea_type": subareaType,
      };
}

class PracticeSubareaPracticeSubarea {
  PracticeSubareaPracticeSubarea({
    this.id,
    this.displayValue,
    this.practiceArea,
  });

  int? id;
  String? displayValue;
  Level? practiceArea;

  factory PracticeSubareaPracticeSubarea.fromRawJson(String str) =>
      PracticeSubareaPracticeSubarea.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PracticeSubareaPracticeSubarea.fromJson(Map<String, dynamic> json) =>
      PracticeSubareaPracticeSubarea(
        id: json["id"],
        displayValue: json["display_value"],
        practiceArea: json["practice_area"] == null
            ? null
            : Level.fromJson(json["practice_area"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "display_value": displayValue,
        "practice_area": practiceArea?.toJson(),
      };
}

class Subsector {
  Subsector({
    this.id,
    this.displayValue,
    this.sector,
  });

  int? id;
  String? displayValue;
  Level? sector;

  factory Subsector.fromRawJson(String str) =>
      Subsector.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Subsector.fromJson(Map<String, dynamic> json) => Subsector(
        id: json["id"],
        displayValue: json["display_value"],
        sector: json["sector"] == null ? null : Level.fromJson(json["sector"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "display_value": displayValue,
        "sector": sector?.toJson(),
      };
}
