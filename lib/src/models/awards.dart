// To parse this JSON data, do
//
//     final awardsRegister = awardsRegisterFromJson(jsonString);

import 'dart:convert';

class Awards {
  Awards({
    required this.id,
    this.title,
    this.description,
    this.instructions,
    this.startDate,
    this.endDate,
    this.projectManager,
  });

  int id;
  String? title;
  String? description;
  String? instructions;
  DateTime? startDate;
  DateTime? endDate;
  ProjectManager? projectManager;

  factory Awards.fromRawJson(String str) => Awards.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Awards.fromJson(Map<String, dynamic> json) => Awards(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        instructions: json["instructions"],
        startDate: json["start_date"] == null
            ? null
            : DateTime.parse(json["start_date"]),
        endDate:
            json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
        projectManager: json["project_manager"] == null
            ? null
            : ProjectManager.fromJson(json["project_manager"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "instructions": instructions,
        "start_date":
            "${startDate!.year.toString().padLeft(4, '0')}-${startDate!.month.toString().padLeft(2, '0')}-${startDate!.day.toString().padLeft(2, '0')}",
        "end_date":
            "${endDate!.year.toString().padLeft(4, '0')}-${endDate!.month.toString().padLeft(2, '0')}-${endDate!.day.toString().padLeft(2, '0')}",
        "project_manager": projectManager?.toJson(),
      };
}

class ProjectManager {
  ProjectManager({
    this.name,
    this.phone,
    this.email,
  });

  String? name;
  dynamic phone;
  String? email;

  factory ProjectManager.fromRawJson(String str) =>
      ProjectManager.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProjectManager.fromJson(Map<String, dynamic> json) => ProjectManager(
        name: json["name"],
        phone: json["phone"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "phone": phone,
        "email": email,
      };
}

/// REGISTER AWARDS
///
///
///
///

class AwardsRegister {
  AwardsRegister({
    required this.sectorAward,
    required this.sector,
    required this.partnerMotivation,
    required this.files,
  });

  int sectorAward;
  int sector;
  String partnerMotivation;
  List<String> files;

  factory AwardsRegister.fromRawJson(String str) =>
      AwardsRegister.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AwardsRegister.fromJson(Map<String, dynamic> json) => AwardsRegister(
        sectorAward: json["sector_award"],
        sector: json["sector"],
        partnerMotivation: json["partner_motivation"],
        files: List<String>.from(json["files"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "sector_award": sectorAward,
        "sector": sector,
        "partner_motivation": partnerMotivation,
        "files": List<dynamic>.from(files.map((x) => x)),
      };
}
