// To parse this JSON data, do
//
//     final referral = referralFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

class Referral {
  Referral({
    required this.name,
    required this.surnames,
    required this.email,
    required this.linkedin,
    required this.comment,
    required this.partnerRelationship,
  });

  String name;
  String surnames;
  String email;
  String linkedin;
  String comment;
  int partnerRelationship;

  factory Referral.fromRawJson(String str) =>
      Referral.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Referral.fromJson(Map<String, dynamic> json) => Referral(
        name: json["name"],
        surnames: json["surnames"],
        email: json["email"],
        linkedin: json["linkedin"],
        comment: json["comment"],
        partnerRelationship: json["partner_relationship"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "surnames": surnames,
        "email": email,
        "linkedin": linkedin,
        "comment": comment,
        "partner_relationship": partnerRelationship,
      };
}

class ReferralRelations {
  ReferralRelations({
    required this.id,
    required this.displayValue,
  });

  int id;
  String displayValue;

  factory ReferralRelations.fromRawJson(String str) =>
      ReferralRelations.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ReferralRelations.fromJson(Map<String, dynamic> json) =>
      ReferralRelations(
        id: json["id"],
        displayValue: json["display_value"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "display_value": displayValue,
      };
}
