import 'dart:convert';
import 'dart:io';
import 'package:ambar_abogados/api/dio_api.dart';
import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/models/profile.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class ReferralService extends ChangeNotifier {
  final String _baseUrlhttp = dotenv.get('API_URL');
  final String _baseUrl = dotenv.get('API_URL_II');

  final storage = const FlutterSecureStorage();

  final Object profiled = {};
  late Referral selectedReferral;

  bool isLoading = true;
  bool isSaving = false;
  bool isSaved = true;

  File? newPictureProfile;

  final client = DioApi();

  /// LOAD REFERRALL RELATIONSHIP LIST
  ///
  ///
  Future<List<ReferralRelations>> loadReferralRelationship() async {
    List response;
    response = await DioApi.httpGet(
        'https://staging.ambarpartners.net/api/partner-referral-relationship/options/',
        options: Options(
          responseType: ResponseType.plain,
        ));

    return (response).map((x) => ReferralRelations.fromJson(x)).toList();
  }

  /// SAVE REFERRAL
  ///
  ///
  // Future<Referral?> saveReferral(Referral bodyReferral) async {
  //   try {
  //     Response response;
  //     isSaved = true;
  //     response = await DioApi.httpPost(
  //       '/api/partner-referral-relationship/options/',
  //       jsonEncode(bodyReferral.toJson()),
  //     );

  //     return Referral.fromJson(jsonDecode(utf8.decode(response.data)));
  //   } catch (e) {
  //     isSaved = false;
  //     return throw Exception('Failed updating referral!!, $e');
  //   }
  // }
  Future<Referral> saveReferral(Referral bodyReferral) async {
    final url = Uri.https(_baseUrl, '/api/partner-referral/');
    final basicAuth = await storage.read(key: 'authbasic');
    isSaved = true;

    final response = await http.post(
      url,
      body: jsonEncode(bodyReferral.toJson()),
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        "Authorization": 'Basic ${basicAuth}'
      },
    );

    try {
      return Referral.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } catch (e) {
      isSaved = false;
      return throw Exception('Failed updating referral!!, $e');
    }
  }

  /// READ TOKEN
  ///
  /// verify if token is stored
  ///
  ///
  Future<String> readToken() async {
    return await storage.read(key: 'token') ?? '';
  }
}
