import 'dart:convert';
import 'dart:io';
import 'package:ambar_abogados/api/dio_api.dart';
import 'package:ambar_abogados/src/models/models.dart';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class AwardsService extends ChangeNotifier {
  final String _baseUrlhttp = dotenv.get('API_URL');
  final String _baseUrl = dotenv.get('API_URL_II');

  final storage = const FlutterSecureStorage();

  final Object profiled = {};
  late Awards selectedAward;

  bool isLoading = true;
  bool isSaving = false;
  bool isSaved = true;

  File? newPictureProfile;

  final client = DioApi();

  /// LOAD AWARDS LIST
  ///
  ///
  Future<List<Awards>> loadAwards() async {
    List response;
    response = await DioApi.httpGet('/api/sector-awards/',
        options: Options(
          responseType: ResponseType.plain,
        ));

    return (response).map((x) => Awards.fromJson(x)).toList();
  }

  /// SAVE AWARD REGISTER
  ///
  ///
  Future<AwardsRegister> saveReferral(AwardsRegister bodyAwards) async {
    final url = Uri.https(_baseUrl, '/api/sector-awards/register/');
    final basicAuth = await storage.read(key: 'authbasic');
    isSaved = true;

    final response = await http.post(
      url,
      body: jsonEncode(bodyAwards.toJson()),
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        "Authorization": 'Basic ${basicAuth}'
      },
    );

    try {
      return AwardsRegister.fromJson(
          jsonDecode(utf8.decode(response.bodyBytes)));
    } catch (e) {
      isSaved = false;
      return throw Exception('Failed creating award!!, $e');
    }
  }

  /// READ TOKEN
  ///
  /// verify if token is stored
  ///
  ///
  Future<String> readToken() async {
    return await storage.read(key: 'token') ?? '';
  }
}
