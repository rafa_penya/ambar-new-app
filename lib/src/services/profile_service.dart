import 'dart:convert';
import 'dart:io';
import 'package:ambar_abogados/src/models/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class ProfileService extends ChangeNotifier {
  final String _baseUrl = dotenv.get('API_URL_II');

  final storage = const FlutterSecureStorage();

  final Object profiled = {};
  late Profile selectedProfile;

  bool isLoading = true;
  bool isSaving = false;
  bool isSaved = true;

  File? newPictureProfile;

  /// READ USER
  ///
  /// Reading user using token validation
  /// and token destructuration
  ///
  ///
  ///
  Future<Profile> getUser() async {
    final url = Uri.https(_baseUrl, '/api/profile/');
    final basicAuth = await storage.read(key: 'authbasic');

    final response = await http.get(
      url,
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        "Authorization": 'Basic ${basicAuth}'
      },
    );

    if (response.statusCode == 200) {
      return Profile.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception('Failed to load album');
    }
  }

  /// UPDATE OR SAVE PROFILE
  ///
  /// Decides if update existent profile or create new profile
  ///
  ///
  ///
  Future saveOrUpdateProfile(Profile profile) async {
    isSaving = true;

    notifyListeners();
    if (profile.email == null) {
      // save profile
      //TODO no update, create
    } else {
      //update profile
      await this.updateProfile(profile);
    }
    isSaving = false;

    notifyListeners();
  }

  /// UPDATE PROFILE
  ///
  ///
  ///
  ///
  Future<Profile> updateProfile(Profile profile) async {
    final url = Uri.https(_baseUrl, '/api/profile/edit/');
    final basicAuth = await storage.read(key: 'authbasic');
    isSaved = true;
    final response = await http.put(
      url,
      body: jsonEncode(profile.toJson()),
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        "Authorization": 'Basic ${basicAuth}'
      },
    );
//TODO "Resolver "la información enviada no era un archivo"
    if (response.statusCode == 200) {
      return Profile.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      isSaved = false;
      throw Exception('Failed updating profile!!');
    }
  }

  void updateSeletedProfileImage(String path) {
    this.selectedProfile.profilePicture = path;
    this.newPictureProfile = File.fromUri(Uri(path: path));
    notifyListeners();
  }

  /// UPDATE AVAILABILITY
  ///
  ///
  ///
  ///
  Future<Profile> updateavailability(
      Profile profileAvailability, String? egetAvailState) async {
    final url = Uri.https(_baseUrl, '/api/profile/edit/');
    final basicAuth = await storage.read(key: 'authbasic');
    isSaved = true;

    profileAvailability.availability = egetAvailState;
    profileAvailability.endAbsence = '2023-03-03';
    profileAvailability.startAbsence = '2023-03-03';

    final response = await http.put(
      url,
      body: jsonEncode(profileAvailability.toJson()),
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        "Authorization": 'Basic ${basicAuth}'
      },
    );

    if (response.statusCode == 200) {
      return Profile.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      isSaved = false;
      throw Exception('Failed updating profile!!');
    }
  }

  /// SAVE AVAILABILITY CONFIG
  ///
  ///
  ///
  ///
  Future<Profile> saveAvailabilityConfig(Profile profileAvailability,
      String? dateinputStart, String? dateinputEnd) async {
    final url = Uri.https(_baseUrl, '/api/profile/edit/');
    final basicAuth = await storage.read(key: 'authbasic');
    isSaved = true;

    profileAvailability.availability = 'low';
    profileAvailability.startAbsence = dateinputStart;
    profileAvailability.endAbsence = dateinputEnd;

    final response = await http.put(
      url,
      body: jsonEncode(profileAvailability.toJson()),
      headers: {
        'Content-type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        "Authorization": 'Basic ${basicAuth}'
      },
    );

    if (response.statusCode == 200) {
      return Profile.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      isSaved = false;
      throw Exception('Failed updating profile!!');
    }
  }

  ///UPLOAD IMAGE AVATAR
  ///
  ///
  ///
  ///
  ///
  //TODO cambiar storing de las imagenes al servidor o bien usar base64
  Future<String?> uploadAvatar() async {
    if (this.newPictureProfile == null) return null;

    this.isSaved = true;
    notifyListeners();

    final url = Uri.parse(
        'https://api.cloudinary.com/v1_1/dvoqqaagd/image/upload?upload_preset=ml_default');

    final imageUploadRequest = http.MultipartRequest('POST', url);
    final file =
        await http.MultipartFile.fromPath('file', this.newPictureProfile!.path);

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print('algo salió mal');
      print(resp.body);
      return null;
    }

    this.newPictureProfile = null;

    final decodedData = json.decode(resp.body);
    return decodedData['secure_url'];
  }

  /// READ TOKEN
  ///
  /// verify if token is stored
  ///
  ///
  Future<String> readToken() async {
    return await storage.read(key: 'token') ?? '';
  }
}
