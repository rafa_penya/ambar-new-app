import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:http/http.dart' as http;

class AuthService extends ChangeNotifier {
  final String _baseUrl = dotenv.get('API_URL');

  final storage = const FlutterSecureStorage();

  // Si retornamos algo, es un error, si no, todo bien!
  /// CREATE USER
  ///
  ///
  ///
  ///
  Future<String?> createUser(String username, String password) async {
    final Map<String, dynamic> authData = {
      'username': username,
      'password': password,
    };

    final url = Uri.https(_baseUrl, '/v1/accounts:signUp');

    final resp = await http.post(url, body: json.encode(authData));
    final Map<String, dynamic> decodedResp = json.decode(resp.body);

    if (decodedResp.containsKey('idToken')) {
      // Token hay que guardarlo en un lugar seguro
      await storage.write(key: 'token', value: decodedResp['idToken']);
      // decodedResp['idToken'];
      return null;
    } else {
      return decodedResp['error']['message'];
    }
  }

  /// LOGIN USER
  ///
  /// Login user into application
  ///
  ///
  ///
  ///
  Future<String?> login(String username, String password) async {
    final Map<String, dynamic> authData = {
      'username': username,
      'password': password
    };

    final url = Uri.https(_baseUrl, '/auth-token/');

    final resp = await http.post(url,
        body: json.encode(authData),
        headers: {'Content-Type': 'application/json'});

    final Map<String, dynamic> decodedResp = json.decode(resp.body);

//encoding credentials for use in storage
    final authBasic = authData['username'] + ':' + authData['password'];
    final bytes = utf8.encode(authBasic);
    final authBasicEncoded = base64.encode(bytes);

    if (decodedResp.containsKey('token')) {
      await storage.write(key: 'authbasic', value: authBasicEncoded);
      await storage.write(key: 'token', value: decodedResp['token']);
      return null;
    } else {
      return decodedResp['error'];
    }
  }

  /// LOGOUT USER
  ///
  /// delete token from store
  ///
  ///
  Future logout() async {
    await storage.delete(key: 'token');
    return;
  }

  /// READ TOKEN
  ///
  /// verify if token is stored
  ///
  ///
  Future<String> readToken() async {
    return await storage.read(key: 'token') ?? '';
  }
}
