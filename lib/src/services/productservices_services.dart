// ignore_for_file: unnecessary_this

import 'dart:convert';
import 'dart:io';

import 'package:ambar_abogados/api/dio_api.dart';
import 'package:dartdoc/dartdoc.dart';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ambar_abogados/src/models/models.dart';
import 'package:http/http.dart' as http;

class ProdServService extends ChangeNotifier {
  final String _baseUrl = dotenv.get('API_URL');
  final List<ProductSer> prods = [];
  late ResultProdSer selectedProdserv;

  final storage = FlutterSecureStorage();
  File? newPictureFile;

  bool isLoading = true;
  bool isSaving = false;

  ProdServService() {
    this.loadProdsServs();
  }

  final client = DioApi();

  /// LOAD PRODUCTS AND SERVICES LIST
  ///
  /// load all producst and services
  ///
  ///
  Future<List<ResultProdSer>> loadProdsServs() async {
    Map<String, dynamic> eventsData = await DioApi.httpGet('/api/products/',
        options: Options(
          responseType: ResponseType.plain,
        ));
    ProductSer prodsers = ProductSer.fromJson(eventsData);
    final List<ResultProdSer> prodsersMap = prodsers.results;
    return prodsersMap;
  }

  /// LOAD PRODUCTS AND SERVICES LIST
  ///
  /// BY CATEGORY (universe)
  ///
  ///
  Future<List<ResultProdSer>> loadProdServByCategory(String category) async {
    Map<String, dynamic> prodsData =
        await DioApi.httpGet('/api/products/?universe=$category',
            options: Options(
              responseType: ResponseType.plain,
            ));
    ProductSer prods = ProductSer.fromJson(prodsData);

    final List<ResultProdSer> prodsMap = prods.results;

    if (prodsMap.length == 0) {
      print('there are not products or services');
    }

    return prodsMap;
  }

  /// LOAD PRODUCT OR SERVICE DETAIL
  ///
  /// BY ID (int)
  ///
  ///
  Future<ResultProdSer> getDetailProdServ(int idProd) async {
    final prodData = await DioApi.httpGet('/api/products/$idProd',
        options: Options(
          responseType: ResponseType.plain,
        ));
    ResultProdSer product = ResultProdSer.fromJson(prodData);

    return product;
  }
}
