// ignore_for_file: unnecessary_this

import 'dart:convert';
import 'dart:io';

import 'package:ambar_abogados/api/dio_api.dart';
import 'package:dartdoc/dartdoc.dart';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ambar_abogados/src/models/models.dart';
import 'package:http/http.dart' as http;

class EventsService extends ChangeNotifier {
  final String _baseUrl = dotenv.get('API_URL');
  final List<Events> events = [];
  late ResultEvents selectedEvent;

  final storage = FlutterSecureStorage();
  File? newPictureFile;

  bool isLoading = true;
  bool isSaving = false;

  EventsService() {
    this.loadEvents();
  }
  final client = DioApi();

  /// LOAD EVENTS LIST
  ///
  /// load all events
  ///
  ///
  Future<List<ResultEvents>> loadEvents() async {
    Map<String, dynamic> eventsData = await DioApi.httpGet('/api/events/',
        options: Options(
          responseType: ResponseType.plain,
        ));
    Events events = Events.fromJson(eventsData);
    final List<ResultEvents> eventsMap = events.results!;
    return eventsMap;
  }

  /// LOAD EVENTS LIST
  ///
  /// BY CATEGORY (universe)
  ///
  ///
  Future<List<ResultEvents>> loadEventsByCategory(String category) async {
    Map<String, dynamic> eventsData =
        await DioApi.httpGet('/api/events/?universe=$category',
            options: Options(
              responseType: ResponseType.plain,
            ));
    Events events = Events.fromJson(eventsData);

    final List<ResultEvents> eventsMap = events.results!;

    if (eventsMap.length == 0) {
      print('there are not events');
    }

    return eventsMap;
  }

  /// LOAD EVENT DETAIL
  ///
  /// BY ID (int)
  ///
  ///
  Future<ResultEvents> getDetailEvent(int idEvent) async {
    //final int? idEvento = idEvent.id;
    final eventData = await DioApi.httpGet('/api/events/$idEvent',
        options: Options(
          responseType: ResponseType.plain,
        ));
    ResultEvents event = ResultEvents.fromJson(eventData);

    // List<ResultEvents> event.products => List<ResultEvents>.from(eventData.map((x) => ResultEvents.fromJson(x)));

    //   Future<List<Post>?> getProducts() async {
    //   try {
    //     var response = await Dio().get('https://fakestoreapi.com/products');
    //     var json = response.data;

    //     if (response.statusCode == 200) {
    //       return postFromJson(json);
    //     } else {
    //       throw Exception("Error");
    //     }
    //   } catch (e) {
    //     throw (e);
    //   }
    // }

    // if (eventsMap.length == 0) {
    //   print('there are not events');
    // }

    return event;
  }
}
