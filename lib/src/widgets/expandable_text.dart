import 'package:ambar_abogados/src/widgets/widgets.dart';

import 'package:flutter/material.dart';

class ExpandableText extends StatefulWidget {
  const ExpandableText({
    super.key,
    this.maxLines = 3,
    required this.detail,
  });

  final String detail;
  final int maxLines;

  @override
  State<ExpandableText> createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final TextPainter textPainter = TextPainter(
          text: TextSpan(
            text: widget.detail,
          ),
          textDirection: Directionality.of(context),
          maxLines: widget.maxLines,
        )..layout(maxWidth: constraints.maxWidth);

        return Column(
          children: <Widget>[
            AnimatedSize(
              duration: const Duration(milliseconds: 400),
              alignment: Alignment.topCenter,
              curve: Curves.easeInOutCubic,
              child: LowSLabel(
                widget.detail,
                maxLines: isExpanded ? null : widget.maxLines,
              ),
            ),
            if (textPainter.didExceedMaxLines) ...[
              TertiaryButton(
                tooltip: isExpanded ? 'less' : 'more',
                icon: isExpanded
                    ? Icon(Icons.expand_less)
                    : Icon(Icons.expand_more),
                color: Colors.transparent,
                onPressed: () => setState(() => isExpanded = !isExpanded),
              ),
            ],
          ],
        );
      },
    );
  }
}
