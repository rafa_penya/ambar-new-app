import 'package:ambar_abogados/src/models/events.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/services/events_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuyableItemView extends StatelessWidget {
  const BuyableItemView({super.key, required this.detailId});
  final int detailId;
  @override
  Widget build(BuildContext context) {
    final eventService = Provider.of<EventsService>(context);
    return Stack(
      children: [
        Positioned(
            child: Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                    offset: Offset(-2, -6),
                    spreadRadius: -6,
                    blurRadius: 6,
                    color: Color.fromRGBO(0, 0, 0, 0.4),
                  )
                ]),
                height: 100,
                width: double.infinity,
                child: Column(
                  children: [
                    FutureBuilder<ResultEvents>(
                        future: eventService.getDetailEvent(detailId),
                        builder: (BuildContext context,
                            AsyncSnapshot<ResultEvents> snapshot) {
                          if (snapshot.hasData) {
                            return Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  _SendButton(dataSend: snapshot.data!)
                                ]);
                          } else if (snapshot.hasError) {
                            return Text('${snapshot.error}');
                          }
                          return LoaderProgresBar();
                        }),
                  ],
                ))),
      ],
    );
  }
}

class _SendButton extends StatelessWidget {
  _SendButton({
    super.key,
    required this.dataSend,
  });

  final ResultEvents dataSend;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 12),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Expanded(
          flex: 4,
          child: Container(
              padding: EdgeInsets.only(left: 12),
              alignment: Alignment.centerLeft,
              child: Text(
                dataSend.id.toString(),
                style: TextStyle(fontSize: 20),
              )),
        ), //TODO recoger el precio seleccionado desde el selector de estados map_products_select
        Expanded(
          flex: 6,
          child: Container(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: Colors.black,
                minimumSize: const Size(131, 44),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              child: const Text('Registrarme'),
              onPressed: () {
                // Navigator.of(context)
                //     .push(createRoute(AvailabilityScreen()));
              },
            ),
          ),
        )
      ]),
    );
  }
}
