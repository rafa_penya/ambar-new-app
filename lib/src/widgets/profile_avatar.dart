import 'dart:io';

import 'package:ambar_abogados/src/models/profile.dart';
import 'package:ambar_abogados/src/providers/profile_form_provider.dart';
import 'package:ambar_abogados/src/services/profile_service.dart';
import 'package:ambar_abogados/src/widgets/font_constants.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class ProfileAvatar extends StatelessWidget {
  const ProfileAvatar({
    super.key,
    required this.profileService,
    required this.datageneral,
    required this.urlimage,
    required this.showname,
  });
  final String? urlimage;
  final ProfileService profileService;
  final Profile datageneral;

  final bool showname;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _SetImage(
          showname: showname,
          profileService: profileService,
          urlimage: urlimage,
          datageneral: datageneral),
    );
  }
}

class _SetImage extends StatelessWidget {
  const _SetImage({
    super.key,
    required this.profileService,
    required this.urlimage,
    required this.datageneral,
    required this.showname,
  });
  final ProfileService profileService;

  final String? urlimage;
  final Profile datageneral;
  final bool showname;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              padding: const EdgeInsets.only(top: 0),
              child: this.urlimage == null
                  ? const CircleAvatar(
                      maxRadius: 50,
                      minRadius: 50,
                      backgroundImage:
                          AssetImage('lib/assets/images/no_image.png'),
                    )
                  : this.urlimage!.startsWith('http')
                      ? CircleAvatar(
                          maxRadius: 50,
                          minRadius: 50,
                          backgroundImage:
                              NetworkImage(datageneral.profilePicture),
                        )
                      : CircleAvatar(
                          maxRadius: 50,
                          minRadius: 50,
                          backgroundImage:
                              AssetImage(datageneral.profilePicture),
                        )),
          Flexible(
            child: showname
                ? Container(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      'Hola, ${datageneral.firstName} ${datageneral.lastName}',
                      style: TextStyles.h2Title.copyWith(color: Colors.black),
                    ),
                  )
                : Container(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text('para modificar la imagen pulse el icono')),
          ),
        ],
      ),
    );
  }
}
