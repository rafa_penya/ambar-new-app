import 'package:ambar_abogados/api/dio_api.dart';
import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/pages/ui/input_decorations.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/events_form_provider.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class EventsWidget extends StatefulWidget {
  @override
  State<EventsWidget> createState() => _EventsWidgetState();
}

class _EventsWidgetState extends State<EventsWidget> {
  @override
  void initState() {
    super.initState();
    initializeDateFormatting('es_ES', null);
  }

  final DioApi client = DioApi();

  @override
  Widget build(BuildContext context) {
    final eventsService = Provider.of<EventsService>(context);

    return FutureBuilder<List<ResultEvents>>(
        future: eventsService.loadEvents(),
        builder:
            (BuildContext context, AsyncSnapshot<List<ResultEvents>> snapshot) {
          if (snapshot.hasData) {
            return Container(
                color: Colors.transparent,
                padding: EdgeInsets.all(0),
                margin: EdgeInsets.all(0),
                width: MediaQuery.of(context).size.width,
                height: 300,
                child: ListView.builder(
                    itemCount: snapshot.data!.length,
                    scrollDirection: Axis.horizontal,
                    //itemExtent: 300,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Container(
                        color: Colors.transparent,
                        width: MediaQuery.of(context).size.width,
                        child: PageView.builder(
                            allowImplicitScrolling: true,
                            itemCount: snapshot.data!.length,
                            padEnds: true,
                            dragStartBehavior: DragStartBehavior.down,
                            physics: const BouncingScrollPhysics(),
                            // store this controller in a State to save the carousel scroll position
                            controller: PageController(viewportFraction: 1),
                            itemBuilder: (BuildContext context, int index) {
                              final data = snapshot.data![index];
                              return _CardEvent(data: data);
                            }),
                      );
                    }));
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const CircularProgressIndicator();
        });
  }
}

/// Event card
///
///
///
class _CardEvent extends StatelessWidget {
  const _CardEvent({
    super.key,
    required this.data,
  });

  final ResultEvents data;

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat.yMMMMd('es_ES');
    final String finalStartDate =
        dateFormatter.format(data.startDate as DateTime);

    return Container(
        padding: EdgeInsets.all(2),
        child: Card(
            color: Colors.transparent,
            elevation: 0,
            shape: RoundedRectangleBorder(
              side: BorderSide.none,
            ),
            margin: EdgeInsets.only(right: 41),
            child: Container(
                child: Column(
              children: [
                Container(
                  height: 170,
                  width: double.infinity,
                  child:
                      // decoration: BoxDecoration(
                      //   image: DecorationImage(
                      //       filterQuality: FilterQuality.low,
                      //       image:

                      CachedNetworkImage(
                          placeholder: (context, url) => LoaderProgresBar(),
                          imageUrl: data.image,
                          imageBuilder: (context, imageProvider) => Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(3),
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )),

                  //       fit: BoxFit.cover),
                  //  ),
                ),
                SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    finalStartDate,
                    style: TextStyles.smallSubtext.copyWith(),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    data.title,
                    style: TextStyles.menuTitle.copyWith(),
                  ),
                ),
              ],
            ))));
  }
}
