import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/pages/drawer/availability/availability_screen.dart';
import 'package:flutter/material.dart';
import 'package:localization/localization.dart';

const List<String> _options = <String>['low', 'medium', 'high'];

const optTranslated = _options;

class RangeSelector extends StatefulWidget {
  const RangeSelector({super.key});

  @override
  State<RangeSelector> createState() => _RangeSelectorState();
}

class _RangeSelectorState extends State<RangeSelector> {
  final List<bool> _selectedOptions = <bool>[false, false, false];

  late String? _optionSelected = '';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // SpecialButton(
        //   key: UniqueKey(),
        //   active: _optionSelected,
        // ),
        Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ToggleButtons(
                    isSelected: _selectedOptions,
                    renderBorder: false,
                    fillColor: Colors.white,
                    color: Colors.black,
                    selectedColor: Colors.black,
                    constraints: const BoxConstraints(
                      minHeight: 40.0,
                      minWidth: 80.0,
                    ),
                    onPressed: (int index) {
                      setState(() {
                        for (int i = 0; i < _selectedOptions.length; i++) {
                          _selectedOptions[i] = i == index;
                          _optionSelected = _options[index] as dynamic;

                          AvailabilityPageBuild.of(context)?.getAvailState =
                              _optionSelected;
                        }
                      });
                    },
                    children: List<Widget>.generate(
                        3,
                        (index) => Container(
                            width: 90,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                width: 1,
                                color: _selectedOptions[index]
                                    ? Colors.black
                                    : Color.fromARGB(255, 207, 207, 207),
                              ),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            padding: const EdgeInsets.all(15),
                            margin: const EdgeInsets.all(10),
                            alignment: Alignment.center,
                            child: Text(_options[index].i18n())))),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

// class SpecialButton extends StatelessWidget {
//   const SpecialButton({required Key key, required this.active})
//       : super(key: key);
//   final String active;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Text(active),
//     );
//   }
// }
