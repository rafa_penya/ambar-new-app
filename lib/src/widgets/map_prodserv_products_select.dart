import 'package:ambar_abogados/src/models/models.dart';
import 'package:flutter/material.dart';

///MAP OF PRODUCT TO SELECT
///
///
///
class MapOfProductsProdServ extends StatefulWidget {
  const MapOfProductsProdServ({
    super.key,
    required this.productsMap,
  });
  final List<Product>?
      productsMap; //TODO aplicar el mapeado correcto al producto de productos y servicios ---> ProductServPurchase
  @override
  State<MapOfProductsProdServ> createState() => _MapOfProductsProdServState();
}

class _MapOfProductsProdServState extends State<MapOfProductsProdServ> {
  String _counter = '';

  @override
  void _buttonChange(String price) {
    setState(() {
      _counter = price;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ...widget.productsMap!.map((reminder) {
        return Container(
            child: reminder.price == "0.00" || reminder.price == null
                ? Text('')
                : Container(
                    child: TextButton(
                    onPressed: () => _buttonChange(reminder.price!),
                    child: Text('boton'),
                  )));
      }).toList(),
      Text(_counter),
      SizedBox(
        height: 250,
      )
    ]);
  }
}
