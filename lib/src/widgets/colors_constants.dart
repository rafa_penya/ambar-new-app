// colors.dart
// Collection of Material colors as static constants
// 2019 Rob Vandelinder

import 'package:flutter/material.dart';

class ColorConstants {
// Colors.grey
  static Color menuIconsColor = Color.fromARGB(255, 0, 0, 0);
  static Color menuIconsColorSelected = const Color(0xFF4FFF30);
  static Color menuBakground = Color(0xFFFFFFFF);
  static Color loginPrimBack = const Color.fromARGB(255, 127, 12, 249);
  static Color scaffBackg = const Color.fromARGB(255, 255, 255, 255);
  static Color buttonsColor = Color(0xFF000000);
  static Color inputBackground = Color.fromRGBO(247, 247, 247, 1);
  static Color menuBackground = Color.fromRGBO(181, 83, 50, 1);
  static Color inactiveColorMenu = Color.fromARGB(255, 227, 179, 164);

  static Color inputBackgroundProf = Color.fromRGBO(237, 237, 237, 1);

  static Color inactiveGreenColorMenu = Color.fromRGBO(129, 149, 145, 1);

  static Color whiteColour = Color.fromRGBO(227, 255, 255, 1);
// Colors.blueGrey
  static Color kColorsBlueGrey100 = const Color(0xFFF5F5F5);
  static Color kColorsBlueGrey200 = const Color(0xFFEEEEEE);
  static Color kColorsBlueGrey300 = const Color(0xFFE0E0E0);
  static Color kColorsBlueGrey400 = const Color(0xFFBDBDBD);
  static Color kColorsBlueGrey = const Color(0xFF9E9E9E);
  static Color kColorsBlueGrey500 = const Color(0xFF9E9E9E);
  static Color kColorsBlueGrey600 = const Color(0xFF757575);
  static Color kColorsBlueGrey700 = const Color(0xFF424242);
  static Color kColorsBlueGrey800 = const Color(0xFF303030);
  static Color kColorsBlueGrey900 = const Color(0xFF212121);

// Colors.blue
  static Color kColorsBlue100 = const Color(0xFFBBDEFB);
  static Color kColorsBlue200 = const Color(0xFF90CAF9);
  static Color kColorsBlue300 = const Color(0xFF64B5F6);
  static Color kColorsBlue400 = const Color(0xFF42A5F5);
  static Color kColorsBlue = const Color(0xFF2196F3);
  static Color kColorsBlue500 = const Color(0xFF2196F3);
  static Color kColorsBlue600 = const Color(0xFF1E88E5);
  static Color kColorsBlue700 = const Color(0xFF1976D2);
  static Color kColorsBlue800 = const Color(0xFF1565C0);
  static Color kColorsBlue900 = const Color(0xFF0D47A1);

// Colors.deepOrange
  static Color kColorsDeepOrange100 = const Color(0xFFFFCCBC);
  static Color kColorsDeepOrange200 = const Color(0xFFFFAB91);
  static Color kColorsDeepOrange300 = const Color(0xFFFF8A65);
  static Color kColorsDeepOrange400 = const Color(0xFFFF7043);
  static Color kColorsDeepOrange = const Color(0xFFFF5722);
  static Color kColorsDeepOrange500 = const Color(0xFFFF5722);
  static Color kColorsDeepOrange600 = const Color(0xFFF4511E);
  static Color kColorsDeepOrange700 = const Color(0xFFE64A19);
  static Color kColorsDeepOrange00 = const Color(0xFFD84315);
  static Color kColorsDeepOrange900 = const Color(0xFFBF360C);

// Colors.red
  static Color kColorsRed100 = const Color(0xFFFFCDD2);
  static Color kColorsRed200 = const Color(0xFFEF9A9A);
  static Color kColorsRed300 = const Color(0xFFE57373);
  static Color kColorsRed400 = const Color(0xFFEF5350);
  static Color kColorsRed = const Color(0xFFF44336);
  static Color kColorsRed500 = const Color(0xFFF44336);
  static Color kColorsRed600 = const Color(0xFFE53935);
  static Color kColorsRed700 = const Color(0xFFD32F2F);
  static Color kColorsRed800 = const Color(0xFFC62828);
  static Color kColorsRed900 = const Color(0xFFB71C1C);

// Colors.pink
  static Color kColorsPink100 = const Color(0xFFF8BBD0);
  static Color kColorsPink200 = const Color(0xFFF48FB1);
  static Color kColorsPink300 = const Color(0xFFF06292);
  static Color kColorsPink400 = const Color(0xFFEC407A);
  static Color kColorsPink = const Color(0xFFE91E63);
  static Color kColorsPink500 = const Color(0xFFE91E63);
  static Color kColorsPink600 = const Color(0xFFD81B60);
  static Color kColorsPink700 = const Color(0xFFC2185B);
  static Color kColorsPink800 = const Color(0xFFAD1457);
  static Color kColorsPink900 = const Color(0xFF880E4F);

// Colors.lime
  static Color kColorsLime100 = const Color(0xFFF0F4C3);
  static Color kColorsLime200 = const Color(0xFFE6EE9C);
  static Color kColorsLime300 = const Color(0xFFCDE775);
  static Color kColorsLime400 = const Color(0xFFD4E157);
  static Color kColorsLime = const Color(0xFFCDDC39);
  static Color kColorsLime500 = const Color(0xFFCDDC39);
  static Color kColorsLime600 = const Color(0xFFC0CA33);
  static Color kColorsLime700 = const Color(0xFFAFB42B);
  static Color kColorsLime800 = const Color(0xFF9E9D24);
  static Color kColorsLime900 = const Color(0xFF827717);

// Colors.teal
  static Color kColorsTeal100 = const Color(0xFFB2DFDB);
  static Color kColorsTeal200 = const Color(0xFF80CBC4);
  static Color kColorsTeal300 = const Color(0xFF4DB6AC);
  static Color kColorsTeal400 = const Color(0xFF26A69A);
  static Color kColorsTeal = const Color(0xFF009688);
  static Color kColorsTeal500 = const Color(0xFF009688);
  static Color kColorsTeal600 = const Color(0xFF00897B);
  static Color kColorsTeal700 = const Color(0xFF00796B);
  static Color kColorsTeal800 = const Color(0xFF00695C);
  static Color kColorsTeal900 = const Color(0xFF004D40);

// Colors.orange
  static Color kColorsOrange100 = const Color(0xFFFFE0B2);
  static Color kColorsOrange200 = const Color(0xFFFFCC80);
  static Color kColorsOrange300 = const Color(0xFFFFB74D);
  static Color kColorsOrange400 = const Color(0xFFFFA726);
  static Color kColorsOrange = const Color(0xFFFF9800);
  static Color kColorsOrange500 = const Color(0xFFFF9800);
  static Color kColorsOrange600 = const Color(0xFFFB8C00);
  static Color kColorsOrange700 = const Color(0xFFE57C00);
  static Color kColorsOrange800 = const Color(0xFFEF6C00);
  static Color kColorsOrange900 = const Color(0xFFE65100);

// Colors.indigo
  static Color kColorsIndigo100 = const Color(0xFFC5CAE9);
  static Color kColorsIndigo200 = const Color(0xFF9FA8DA);
  static Color kColorsIndigo300 = const Color(0xFF7986CB);
  static Color kColorsIndigo400 = const Color(0xFF5C6BC0);
  static Color kColorsIndigo = const Color(0xFF3F51B5);
  static Color kColorsIndigo500 = const Color(0xFF3F51B5);
  static Color kColorsIndigo600 = const Color(0xFF3949AB);
  static Color kColorsIndigo700 = const Color(0xFF303F9F);
  static Color kColorsIndigo800 = const Color(0xFF283593);
  static Color kColorsIndigo900 = const Color(0xFF1A237E);

// Colors.green
  static Color kColorsGreen100 = const Color(0xFFC8E6C9);
  static Color kColorsGreen200 = const Color(0xFFA5D6A7);
  static Color kColorsGreen300 = const Color(0xFF81C784);
  static Color kColorsGreen400 = const Color(0xFF66BB6A);
  static Color kColorsGreen = const Color(0xFF4CAF50);
  static Color kColorsGreen500 = const Color(0xFF4CAF50);
  static Color kColorsGreen600 = const Color(0xFF43A047);
  static Color kColorsGreen700 = const Color(0xFF388E3C);
  static Color kColorsGreen800 = const Color(0xFF2E7D32);
  static Color kColorsGreen900 = const Color(0xFF1B5E20);

// Colors.amber
  static Color kColorsAmber100 = const Color(0xFFFFECB3);
  static Color kColorsAmber200 = const Color(0xFFFFE082);
  static Color kColorsAmber300 = const Color(0xFFFFD54F);
  static Color kColorsAmber400 = const Color(0xFFFFCA28);
  static Color kColorsAmber = const Color(0xFFFFC107);
  static Color kColorsAmber500 = const Color(0xFFFFC107);
  static Color kColorsAmber600 = const Color(0xFFFFB330);
  static Color kColorsAmber700 = const Color(0xFFFFA000);
  static Color kColorsAmber800 = const Color(0xFFFF8F00);
  static Color kColorsAmber900 = const Color(0xFFFF6F00);

// Colors.lightBlue
  static Color kColorsLightBlue100 = const Color(0xFFB3E5FC);
  static Color kColorsLightBlue200 = const Color(0xFF81D4FA);
  static Color kColorsLightBlue300 = const Color(0xFF4FC3F7);
  static Color kColorsLightBlue400 = const Color(0xFF29B6F6);
  static Color kColorsLightBlue = const Color(0xFF03A9F4);
  static Color kColorsLightBlue500 = const Color(0xFF03A9F4);
  static Color kColorsLightBlue600 = const Color(0xFF039BE5);
  static Color kColorsLightBlue700 = const Color(0xFF0288D1);
  static Color kColorsLightBlue800 = const Color(0xFF0277BD);
  static Color kColorsLightBlue900 = const Color(0xFF01579B);
}
