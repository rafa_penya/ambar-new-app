import 'package:flutter/material.dart';

class TertiaryButton extends StatelessWidget {
  final Icon icon;
  final VoidCallback? onPressed;
  final bool isLoading;
  final Color color;
  final Color iconColor;
  final double size;
  final double iconSize;
  final String tooltip;
  final bool enableOnPressed;

  const TertiaryButton({
    super.key,
    required this.icon,
    required this.onPressed,
    this.isLoading = false,
    this.color = Colors.green,
    this.iconColor = Colors.amberAccent,
    this.size = 40,
    this.iconSize = 20,
    required this.tooltip,
    this.enableOnPressed = true,
  });

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: isLoading,
      child: Tooltip(
        message: tooltip,
        waitDuration: const Duration(seconds: 1),
        child: MaterialButton(
          elevation: 0,
          padding: size > 56 ? EdgeInsets.all(size / 4) : null,
          focusElevation: 0,
          hoverElevation: 0,
          highlightElevation: 0,
          height: size,
          minWidth: size,
          color: color,
          splashColor: iconColor.withOpacity(0.25),
          disabledColor: color.withOpacity(0.4),
          disabledTextColor: iconColor.withOpacity(0.4),
          shape: const CircleBorder(),
          onPressed:
              enableOnPressed && onPressed != null ? () => onPressed!() : null,
          child: icon,
        ),
      ),
    );
  }
}
