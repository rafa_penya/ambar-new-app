import 'package:flutter/material.dart';

class Fonts {
  static const String roboto = 'Roboto';
  static const String gtSuperDisplay = 'GT Super Display';
}

class TextStyles {
  static const h1Title = TextStyle(
    fontFamily: Fonts.gtSuperDisplay,
    fontWeight: FontWeight.w500,
    fontSize: 46,
    height: 1.1,
    letterSpacing: -0.1,
  );

  static const h2Title = TextStyle(
    fontFamily: Fonts.gtSuperDisplay,
    fontWeight: FontWeight.w500,
    fontSize: 40,
    height: 1.12,
    letterSpacing: -0.1,
  );

  static const h4Title = TextStyle(
    fontFamily: Fonts.gtSuperDisplay,
    fontWeight: FontWeight.w500,
    fontSize: 28,
    height: 1.12,
    letterSpacing: -0.1,
  );

  static const menuTitle = TextStyle(
    fontFamily: Fonts.gtSuperDisplay,
    fontWeight: FontWeight.w500,
    fontSize: 22,
    height: 1.12,
    letterSpacing: -0.1,
  );

  static const normalText = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    fontSize: 14,
    height: 1.13,
    letterSpacing: -0.1,
  );

  static const normalTextUnderline = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    decoration: TextDecoration.underline,
    fontSize: 14,
    height: 1.13,
    letterSpacing: -0.1,
  );

  static const smallSubtext = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    fontSize: 12,
    height: 1.13,
    letterSpacing: -0.1,
  );

  static const middleSubtext = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    fontSize: 16,
    height: 1.13,
    letterSpacing: -0.1,
  );

  static const middleSubtextUnderline = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    decoration: TextDecoration.underline,
    fontSize: 16,
    height: 1.13,
    letterSpacing: -0.1,
  );

  static const bigSubtext = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    fontSize: 18,
    height: 1.13,
    letterSpacing: -0.1,
  );

  static const bigRobotoTitle = TextStyle(
    fontFamily: Fonts.roboto,
    fontWeight: FontWeight.normal,
    fontSize: 28,
    height: 1.13,
    letterSpacing: -0.1,
  );
}
