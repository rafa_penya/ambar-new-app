import 'package:flutter/material.dart';

class LowSLabel extends StatelessWidget {
  final String text;
  final Color? color;
  final TextAlign? textAlign;
  final bool isBold;
  final int? maxLines;

  const LowSLabel(
    this.text, {
    super.key,
    this.textAlign,
    this.color,
    this.isBold = false,
    this.maxLines,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      softWrap: true,
      overflow: TextOverflow.fade,
      maxLines: maxLines,
    );
  }
}
