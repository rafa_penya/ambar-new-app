import 'package:flutter/material.dart';

final ThemeData AmbarTheme = ThemeData.light().copyWith(
  errorColor: shrineErrorRed,
  backgroundColor: shrinePink100,
);

const Color shrinePink100 = Color.fromRGBO(193, 193, 193, 0.8);
const Color shrineErrorRed = Color(0xFFC5032B);
