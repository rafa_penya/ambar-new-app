import 'package:ambar_abogados/src/models/profile.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/profile_form_provider.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:provider/provider.dart';

class ProfileTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final profileForm = Provider.of<ProfileFormProvider>(context);

    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        endDrawer: SideBarNavigation(),
        body: Stack(
          children: <Widget>[
            CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate([
                  ///HOME CONTENT
                  ProfileGrid(),
                ]))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

///HOME GRID CREATION
class ProfileGrid extends StatefulWidget {
  ProfileGrid();

  @override
  _ProfileGridState createState() => _ProfileGridState();
}

class _ProfileGridState extends State<ProfileGrid> {
  ScrollController controller = ScrollController();
  double scrollAnterior = 0;
  late final ProfileFormProvider profileForm;
  @override
  void initState() {
    controller.addListener(() {
      if (controller.offset > scrollAnterior && controller.offset > 150) {
        Provider.of<_MenuModel>(context, listen: false).mostrar =
            true; //en false si queremos que desaparezca al hacer scroll
      } else {
        Provider.of<_MenuModel>(context, listen: false).mostrar = true;
      }
      scrollAnterior = controller.offset;
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);
    // final profileForm = Provider.of<ProfileFormProvider>(context);

    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [
          FutureBuilder<Profile>(
            future: profileService.getUser(),
            builder: (BuildContext context, AsyncSnapshot<Profile> snapshot) {
              if (snapshot.hasData) {
                final datageneral = snapshot.data!;
                return Column(
                  children: [
                    ProfileAvatar(
                      showname: true,
                      profileService: profileService,
                      datageneral: datageneral,
                      urlimage: datageneral.profilePicture,
                    ),
                    _MyProfile(profileData: snapshot.data!),
                    Container(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        width: double.infinity,
                        alignment: Alignment.centerLeft,
                        child: _CO2Impact()),
                    const SizedBox(height: 150),
                  ],
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const CircularProgressIndicator(
                color: Colors.amber,
              );
            },
          ),
        ],
      ),
    );
  }
}

///MY PROFILE BLOCK
class _MyProfile extends StatelessWidget {
  // final int index;
  _MyProfile({required this.profileData});
  final Profile profileData;
  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            width: double.infinity,
            alignment: Alignment.centerLeft,
            child: _ProfileTabs(profileData: profileData)),
      ],
    );
  }
}

///Co2 impact BLOCK
class _CO2Impact extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.outline,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          color: const Color.fromRGBO(40, 73, 67, 1),
          margin: const EdgeInsets.only(top: 40, bottom: 8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(height: 30),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                width: double.infinity,
                child: Text(
                  'Impacto cero',
                  style: TextStyles.h4Title.copyWith(color: Colors.white),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        'Calculamos les emisiones de CO2 que producimos en cada proyecto para conseguir un impa c sto cero',
                        style: TextStyles.middleSubtext
                            .copyWith(color: ColorConstants.whiteColour),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 00),
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        Text(
                          '10.045',
                          style: TextStyles.h2Title
                              .copyWith(color: ColorConstants.whiteColour),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          'Árboles plantados',
                          style: TextStyles.normalText.copyWith(
                              fontWeight: FontWeight.bold,
                              color: ColorConstants.whiteColour),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50),
            ],
          )),
    );
  }
}

///PROFILE TABS
///
///

class _ProfileTabs extends StatefulWidget {
  _ProfileTabs({required this.profileData});
  final Profile profileData;

  @override
  State<_ProfileTabs> createState() => _ProfileTabsWidgetState();
}

class _ProfileTabsWidgetState extends State<_ProfileTabs>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final profData = widget.profileData;

    final dateFormatter = DateFormat('dd/MM/yyyy');

    final String finalBirthDate =
        dateFormatter.format(profData.dateOfBirth as DateTime);

    final profileService = Provider.of<ProfileService>(context);
    return Column(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: 35,
              margin: const EdgeInsets.only(bottom: 12),
              width: double.infinity,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey),
                ),
              ),
              child: TabBar(
                indicatorColor: Colors.transparent,
                indicatorWeight: 1,
                indicatorPadding:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                indicatorSize: TabBarIndicatorSize.tab,
                labelColor: Colors.black,
                labelPadding: const EdgeInsets.only(left: 2),
                unselectedLabelColor: Colors.grey,
                controller: _tabController,
                isScrollable: true,
                tabs: const <Widget>[
                  Tab(
                    child: SizedBox(
                      width: 70,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Personal",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      width: 90,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Profesional",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Column(
          children: [
            Container(
              alignment: Alignment.topLeft,
              height: 800,
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  _PersonalTab(
                      profData: profData, finalBirthDate: finalBirthDate),
                  Center(
                    //profesional tab
                    child: Text("It's rainy here",
                        style:
                            TextStyles.h4Title.copyWith(color: Colors.black)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

/// PERSONAL TAB
///
/// Content from personal user info
///
///
class _PersonalTab extends StatelessWidget {
  const _PersonalTab({
    super.key,
    required this.profData,
    required this.finalBirthDate,
  });

  final Profile profData;
  final String finalBirthDate;

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);

    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 0),
        alignment: Alignment.topLeft,
        margin: const EdgeInsets.only(top: 20, bottom: 8),
        child: Container(
          alignment: Alignment.topLeft,
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 20),
                alignment: Alignment.topLeft,
                child: Text(
                  'Información personal',
                  style: TextStyles.h4Title.copyWith(color: Colors.black),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        'Mis datos',
                        style:
                            TextStyles.bigSubtext.copyWith(color: Colors.black),
                      ),
                    ),
                    Container(
                      width: 40,
                      alignment: Alignment.centerRight,
                      child: ElevatedButton(
                        onPressed: () {
                          profileService.selectedProfile = profData;

                          Navigator.of(context)
                              .push(createRoute(EditProfile()));
                        },
                        style: ElevatedButton.styleFrom(
                          shape: const CircleBorder(),
                          elevation: 0,
                          alignment: const Alignment(2, 0),
                          padding: const EdgeInsets.only(right: 14),
                          backgroundColor: Colors.black, // <-- Button color
                          foregroundColor: Colors.red, // <-- Splash color
                        ),
                        child: const Icon(
                            size: 18, Icons.edit, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  alignment: Alignment.topLeft,
                  child: Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        WidgetRowProfile(
                            labelData: 'Nombre',
                            profData:
                                '${profData.firstName} ${profData.lastName}'),
                        WidgetRowProfile(
                            labelData: 'E-mail', profData: profData.email),
                        WidgetRowProfile(
                            labelData: 'Teléfono', profData: profData.phone),
                        WidgetRowProfile(
                            labelData: 'Fecha de nacimiento',
                            profData: finalBirthDate),
                        WidgetRowProfile(
                            labelData: 'Dirección',
                            profData:
                                '${profData.street}, ${profData.zipCode}, ${profData.province}'),
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 30),
                          alignment: Alignment.topLeft,
                          child: ElevatedButton(
                            style: OutlinedButton.styleFrom(
                                minimumSize: const Size(131, 44),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50)),
                                backgroundColor:
                                    const Color.fromRGBO(0, 0, 0, 1)),
                            child: const Text('Cambiar contraseña'),
                            onPressed: () {
                              Navigator.of(context)
                                  .push(createRoute(ChangePassword()));
                            },
                          ),
                        ),
                      ],
                    ),
                  )),
              Container(
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      child: Text(
                        'Mis intereses',
                        style:
                            TextStyles.bigSubtext.copyWith(color: Colors.black),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 30),
                      width: double.infinity,
                      child: Center(
                        child: Column(
                          children: [
                            Container(
                              height: 50,
                              child: Center(
                                child: Image(
                                  image: AssetImage(
                                      'lib/assets/images/yellow-circle.png'),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(bottom: 25),
                              child: Center(
                                child: Text(
                                  'Este campo está vacío',
                                  style: TextStyles.smallSubtext
                                      .copyWith(color: Colors.black),
                                ),
                              ),
                            ),
                            Container(
                              child: Center(
                                child: ElevatedButton(
                                  style: OutlinedButton.styleFrom(
                                      side: BorderSide(
                                          width: 1, color: Colors.black),
                                      minimumSize: const Size(131, 44),
                                      foregroundColor: Colors.black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      backgroundColor: const Color.fromRGBO(
                                          255, 255, 255, 1)),
                                  child: const Text('Añadir'),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .push(createRoute(AddInterests()));
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
