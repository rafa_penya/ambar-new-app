import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DiscountCard extends StatelessWidget {
  const DiscountCard({super.key, required this.data});

  final ResultEvents data;

  @override
  Widget build(BuildContext context) {
    final eventsService = Provider.of<EventsService>(context);

    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        decoration: _cardBorders(),
        width: double.infinity,
        child: Stack(children: [Container(child: _CardEvent(data: data))]),
      ),
    );
  }

  BoxDecoration _cardBorders() =>
      const BoxDecoration(color: Colors.transparent);
}

/// Event card
///
///
///
class _CardEvent extends StatelessWidget {
  const _CardEvent({
    super.key,
    required this.data,
  });

  final ResultEvents data;

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat.yMMMMd('es_ES');
    final String finalStartDate =
        dateFormatter.format(data.startDate as DateTime);

    return Container(
        padding: const EdgeInsets.all(0),
        child: Card(
            color: Colors.transparent,
            elevation: 0,
            shape: const RoundedRectangleBorder(
              side: BorderSide.none,
            ),
            child: Container(
                child: Column(
              children: [
                SizedBox(
                  height: 180,
                  width: double.infinity,
                  child: CachedNetworkImage(
                      placeholder: (context, url) => Container(
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    'lib/assets/images/loading1.gif'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                      imageUrl: data.image,
                      imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    finalStartDate,
                    style: TextStyles.smallSubtext.copyWith(),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Flexible(
                    child: RichText(
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      strutStyle: const StrutStyle(),
                      text: TextSpan(
                        style:
                            TextStyles.menuTitle.copyWith(color: Colors.black),
                        text: data.title,
                      ),
                    ),
                  ),
                ),
              ],
            ))));
  }
}
