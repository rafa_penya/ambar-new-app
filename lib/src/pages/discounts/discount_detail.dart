import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DiscountDetailScreen extends StatefulWidget {
  DiscountDetailScreen({super.key});

  @override
  State<DiscountDetailScreen> createState() => _DiscountDetailScreenState();
}

class _DiscountDetailScreenState extends State<DiscountDetailScreen> {
  @override
  void initState() {
    super.initState();

    initializeDateFormatting('es_ES', null);
  }

  @override
  Widget build(BuildContext context) {
    final eventService = Provider.of<EventsService>(context);

    final int detailId = eventService.selectedEvent.id;

    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        bottomSheet: BuyableItemView(detailId: detailId),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(35),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 70),
                  width: double.infinity,
                  child: Column(
                    children: [
                      Container(
                          width: double.infinity,
                          child: Container(child: CloseButtonX(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ))),
                    ],
                  ),
                ),
                FutureBuilder<ResultEvents>(
                    future: eventService.getDetailEvent(detailId),
                    builder: (BuildContext context,
                        AsyncSnapshot<ResultEvents> snapshot) {
                      if (snapshot.hasData) {
                        return Column(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              _DiscountDetailContent(detail: snapshot.data!),
                            ]);
                      } else if (snapshot.hasError) {
                        return Text('${snapshot.error}');
                      }
                      return const LinearProgressIndicator();
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

///EVENT DETAIL CONTENT
///
///
///
///
///
class _DiscountDetailContent extends StatelessWidget {
  const _DiscountDetailContent({
    super.key,
    required this.detail,
  });

  final ResultEvents detail;

  @override
  Widget build(BuildContext context) {
    List<Product>? productsMap = detail.products;

    return SingleChildScrollView(
      child: Container(
        alignment: Alignment.topLeft,
        padding: const EdgeInsets.only(top: 25, bottom: 35),
        child: Column(
          children: [
            Container(
                height: 420,
                width: double.infinity,
                child: _EventPicture(detail.image)),
            Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: detail.isOrganizedByAmbar
                  ? Text(
                      'Organizado por Ambar Partners',
                      style: TextStyles.middleSubtext.copyWith(fontSize: 14),
                    )
                  : null,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(top: 0, bottom: 20),
              child: Text(
                detail.title,
                style: TextStyles.h4Title.copyWith(fontSize: 34),
              ),
            ),
            Container(
                padding: const EdgeInsets.all(0),
                alignment: Alignment.topLeft,
                child: ExpandableText(detail: detail.description)),
            _EventDataPurchase(detail: detail),
            MapOfProducts(productsMap: productsMap),
          ],
        ),
      ),
    );
  }
}

///EVENT DATA PURCHASE
///
///
///
///
///
class _EventDataPurchase extends StatelessWidget {
  _EventDataPurchase({
    super.key,
    required this.detail,
  });

  final ResultEvents detail;

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat.yMMMMd('es_ES');

    final String finalStartDate =
        dateFormatter.format(detail.startDate as DateTime);

    return Column(
      children: [
        Container(
            padding: const EdgeInsets.all(3.0),
            alignment: Alignment.topLeft,
            child: const Text('Archivos adjuntos')),
        Container(
            alignment: Alignment.topLeft,
            child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                elevation: 0,
                color: ColorConstants.kColorsBlueGrey200,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 25, horizontal: 10),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        _DataRow(
                            altura: 60,
                            dataInfo: finalStartDate,
                            dataAdded: detail.dateDescription!,
                            icon: Icons.watch_later_outlined),
                        _DataRow(
                            altura: 60,
                            dataInfo: detail.places!.toString(),
                            dataAdded:
                                'Quedan ${detail.places! - detail.currentPurchasedPlaces!}',
                            icon: Icons.people),
                        _DataRow(
                            altura: 90,
                            dataInfo: detail.addressName!,
                            dataAdded: detail.address!,
                            icon: Icons.map_outlined),
                        const _DataRow(
                            altura: 55,
                            dataInfo: 'Asistencia',
                            dataAdded: 'Ya estás registrado en este evento',
                            icon: Icons.check),
                      ]),
                )))
      ],
    );
  }
}

///DATA ROW
///
///
class _DataRow extends StatelessWidget {
  const _DataRow({
    super.key,
    required this.dataInfo,
    required this.icon,
    required this.dataAdded,
    required this.altura,
  });

  final String dataInfo;
  final double altura;
  final IconData icon;
  final String dataAdded;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: altura,
      child: Row(
        children: [
          Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(left: 15, right: 10),
              child: Icon(icon)),
          Flexible(
              child: Container(
                  padding: const EdgeInsets.only(left: 10, right: 25),
                  child: Column(
                    children: [
                      Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            dataInfo,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )),
                      Container(
                          alignment: Alignment.centerLeft,
                          child: Text(dataAdded)),
                    ],
                  ))),
        ],
      ),
    );
  }
}

/// EVENT PICTURE
///
///
///
///
class _EventPicture extends StatelessWidget {
  const _EventPicture(
    this.image, {
    super.key,
  });
  final String image;
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
        placeholder: (context, url) => Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('lib/assets/images/loading1.gif'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
        imageUrl: image,
        errorWidget: (context, url, error) => Icon(Icons.error),
        imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ));
  }
}

///MENU MODEL
///
///
///
///
///
///
///
class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
