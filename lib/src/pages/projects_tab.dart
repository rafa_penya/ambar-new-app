import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class ProjectsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        endDrawer: SideBarNavigation(),
        body: Stack(
          children: <Widget>[
            CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate([
                  ///HOME CONTENT
                  ProjectsGrid(),
                ]))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

///HOME GRID CREATION
class ProjectsGrid extends StatefulWidget {
  @override
  _ProjectsGridState createState() => _ProjectsGridState();
}

class _ProjectsGridState extends State<ProjectsGrid> {
  ScrollController controller = ScrollController();
  double scrollAnterior = 0;

  @override
  void initState() {
    controller.addListener(() {
      if (controller.offset > scrollAnterior && controller.offset > 150) {
        Provider.of<_MenuModel>(context, listen: false).mostrar =
            true; //en false si queremos que desaparezca al hacer scroll
      } else {
        Provider.of<_MenuModel>(context, listen: false).mostrar = true;
      }
      scrollAnterior = controller.offset;
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          // Container(
          //     margin: const EdgeInsets.only(top: 25),
          //     child: const SizedBox(
          //       child: BackerButton(urlBack: 'login'),
          //     )),
          Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 8),
                child: Center(
                  child: Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Text(
                          'Colaboraciones a ',
                          style:
                              TextStyles.h2Title.copyWith(color: Colors.black),
                        ),
                        Text(
                          'tu medida ',
                          style:
                              TextStyles.h2Title.copyWith(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              _CheckAvailability(),
              _MyProjects(),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  child: _CO2Impact()),
              const SizedBox(height: 150),
            ],
          ),
        ],
      ),
    );
  }
}

///CHECK AVAILABILITY BLOCK
class _CheckAvailability extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context)
                          .push(createRoute(AvailabilityScreen()));
                    },
                    child: Icon(
                        size: 18, Icons.notifications, color: Colors.black),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(), elevation: 0,
                      padding: EdgeInsets.all(3),
                      backgroundColor:
                          ColorConstants.kColorsBlueGrey300, // <-- Button color
                      foregroundColor: Colors.red, // <-- Splash color
                    ),
                  ),
                  Text(
                    'Tu disponibilidad Baja',
                    style:
                        TextStyles.middleSubtext.copyWith(color: Colors.black),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}

///MY PROJECTS BLOCK
class _MyProjects extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 40, bottom: 8),
            child: SizedBox(
              child: Text(
                'Mis proyectos',
                style: TextStyles.h4Title.copyWith(color: Colors.black),
              ),
            )),
        Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            width: double.infinity,
            alignment: Alignment.centerLeft,
            child: _ProjectsTabs()),
      ],
    );
  }
}

///Co2 impact BLOCK
class _CO2Impact extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.outline,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          color: const Color.fromRGBO(40, 73, 67, 1),
          margin: const EdgeInsets.only(top: 40, bottom: 8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(height: 30),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                width: double.infinity,
                child: Text(
                  'Impacto cero',
                  style: TextStyles.h4Title.copyWith(color: Colors.white),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        'Calculamos les emisiones de CO2 que producimos en cada proyecto para conseguir un impa c sto cero',
                        style: TextStyles.middleSubtext
                            .copyWith(color: ColorConstants.whiteColour),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 00),
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        Text(
                          '10.045',
                          style: TextStyles.h2Title
                              .copyWith(color: ColorConstants.whiteColour),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          'Árboles plantados',
                          style: TextStyles.normalText.copyWith(
                              fontWeight: FontWeight.bold,
                              color: ColorConstants.whiteColour),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50),
            ],
          )),
    );
  }
}

///DISCOUNTS BLOCK
class _DiscountsLifesrtyle extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 40, bottom: 8),
              child: SizedBox(
                child: Text(
                  'Descuentos',
                  style: TextStyles.h4Title.copyWith(color: Colors.black),
                ),
              )),
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 0, bottom: 8),
              child: SizedBox(
                child: Text(
                  'Productos exclusivos para nuestros socios',
                  style: TextStyles.middleSubtext.copyWith(color: Colors.black),
                ),
              )),
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 0, bottom: 8),
              child: SizedBox(
                child: Text(
                  'Ver todos',
                  style: TextStyles.normalTextUnderline
                      .copyWith(color: Colors.black),
                ),
              )),
          Card(
              elevation: 0,
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: Theme.of(context).colorScheme.outline,
                ),
                borderRadius: const BorderRadius.all(Radius.circular(6)),
              ),
              color: const Color.fromRGBO(250, 50, 55, 1),
              margin: const EdgeInsets.only(top: 15, bottom: 8),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                    width: double.infinity,
                    child: Text(
                      'Febrero',
                      style: TextStyles.smallSubtext.copyWith(
                          color: ColorConstants.inactiveGreenColorMenu),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}

///PROJECTS TABS
///
///

class _ProjectsTabs extends StatefulWidget {
  const _ProjectsTabs({super.key});

  @override
  State<_ProjectsTabs> createState() => _ProjectsTabsWidgetState();
}

class _ProjectsTabsWidgetState extends State<_ProjectsTabs>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Container(
            //   decoration: BoxDecoration(
            //     border: Border(
            //       bottom: BorderSide(color: Colors.black, width: 2.0),
            //     ),
            //   ),
            // ),
            Container(
              height: 35,
              margin: EdgeInsets.only(bottom: 12),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey),
                ),
              ),
              child: TabBar(
                indicatorColor: Colors.transparent,
                indicatorWeight: 1,
                indicatorPadding:
                    EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                indicatorSize: TabBarIndicatorSize.tab,
                labelColor: Colors.black,
                labelPadding: EdgeInsets.only(left: 2),
                unselectedLabelColor: Colors.grey,
                controller: _tabController,
                isScrollable: true,
                tabs: const <Widget>[
                  Tab(
                    child: SizedBox(
                      width: 70,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Activos",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      width: 90,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Finalizados",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      width: 110,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Oportunidades",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Column(
          children: [
            Container(
              height: 150,
              child: TabBarView(
                controller: _tabController,
                children: const <Widget>[
                  Center(
                    child: Text("It's cloudy here"),
                  ),
                  Center(
                    child: Text("It's rainy here"),
                  ),
                  Center(
                    child: Text("It's sunny here"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
