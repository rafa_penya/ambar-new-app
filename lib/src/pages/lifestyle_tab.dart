import 'package:ambar_abogados/src/pages/discounts/discounts_screen.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/pages/products/products.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class LifeStyle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        endDrawer: SideBarNavigation(),
        body: Stack(
          children: <Widget>[
            CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate([
                  ///HOME CONTENT
                  LifeStyleGrid(),
                ]))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

///HOME GRID CREATION
class LifeStyleGrid extends StatefulWidget {
  @override
  _LifeStyleGridState createState() => _LifeStyleGridState();
}

class _LifeStyleGridState extends State<LifeStyleGrid> {
  ScrollController controller = ScrollController();
  double scrollAnterior = 0;

  @override
  void initState() {
    controller.addListener(() {
      if (controller.offset > scrollAnterior && controller.offset > 150) {
        Provider.of<_MenuModel>(context, listen: false).mostrar =
            true; //en false si queremos que desaparezca al hacer scroll
      } else {
        Provider.of<_MenuModel>(context, listen: false).mostrar = true;
      }
      scrollAnterior = controller.offset;
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Container(
          //     margin: const EdgeInsets.only(top: 25),
          //     child: const SizedBox(
          //       child: BackerButton(urlBack: 'login'),
          //     )),
          Column(
            children: [
              Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(top: 20, bottom: 8),
                  child: SizedBox(
                    child: Text(
                      'Lifestyle',
                      style: TextStyles.h1Title.copyWith(color: Colors.black),
                    ),
                  )),
              Container(
                color: Colors.transparent,
                margin: const EdgeInsets.only(top: 15, bottom: 40),
                child: SizedBox(
                  child: Row(
                    children: [
                      Text(
                        '“',
                        style: TextStyles.h1Title.copyWith(fontSize: 40),
                      ),
                      const Flexible(
                          child: Text(
                        'Un oasis para tu rutina',
                        style: TextStyle(fontSize: 19),
                      )),
                      Text(
                        '”',
                        style: TextStyles.h1Title.copyWith(fontSize: 40),
                      ),
                    ],
                  ),
                ),
              ),

              _EventsLifesrtyle(),

              _ProductsServiLifesrtyle(),
              _DiscountsLifesrtyle()

              //SliderCards()
            ],
          ),
        ],
      ),
    );
  }
}

///EVENTS BLOCK
class _EventsLifesrtyle extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 10, bottom: 8),
              child: SizedBox(
                child: Text(
                  'Eventos',
                  style: TextStyles.h4Title.copyWith(color: Colors.black),
                ),
              )),
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 0, bottom: 0),
              child: SizedBox(
                child: Text(
                  'Próximos eventos destacados',
                  style: TextStyles.middleSubtext.copyWith(color: Colors.black),
                ),
              )),
          Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(left: 3),
              margin: const EdgeInsets.only(top: 0, bottom: 8),
              child: SizedBox(
                child: TextButton(
                  style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      alignment: Alignment.centerLeft),
                  onPressed: () {
                    Navigator.of(context).push(createRoute(EventsScreen()));
                  },
                  child: Text(
                    'Ver todos',
                    style: TextStyles.normalTextUnderline
                        .copyWith(color: Colors.black),
                  ),
                ),
              )),
          Card(
              color: Colors.transparent,
              elevation: 0,
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: Theme.of(context).colorScheme.outline,
                ),
                borderRadius: const BorderRadius.all(Radius.circular(0)),
              ),
              margin: const EdgeInsets.only(top: 10, bottom: 8),
              child: Column(
                children: [
                  Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 0, vertical: 0),
                      child: EventsWidget()),
                ],
              )),
        ],
      ),
    );
  }
}

///PRODUCTS ANS SERVICES BLOCK
class _ProductsServiLifesrtyle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.transparent,
        height: 380,
        child: Column(
          children: [
            Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(top: 10, bottom: 8),
                child: SizedBox(
                  child: Text(
                    'Servicios y productos',
                    style: TextStyles.h4Title.copyWith(color: Colors.black),
                  ),
                )),
            Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(top: 0, bottom: 0),
                child: SizedBox(
                  child: Text(
                    'Productos exclusivos para nuestros socios',
                    style:
                        TextStyles.middleSubtext.copyWith(color: Colors.black),
                  ),
                )),
            Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 3),
                margin: const EdgeInsets.only(top: 0, bottom: 0),
                child: SizedBox(
                  child: TextButton(
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        alignment: Alignment.centerLeft),
                    onPressed: () {
                      Navigator.of(context)
                          .push(createRoute(ServicesProductsScreen()));
                    },
                    child: Text(
                      'Ver todos',
                      style: TextStyles.normalTextUnderline
                          .copyWith(color: Colors.black),
                    ),
                  ),
                )),
            Card(
                color: Colors.transparent,
                elevation: 0,
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Theme.of(context).colorScheme.outline,
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(0)),
                ),
                margin: const EdgeInsets.only(top: 10, bottom: 8),
                child: Column(
                  children: [
                    Container(
                        color: Colors.transparent,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 0),
                        child: ProductServicesWidget()),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}

///DISCOUNTS BLOCK
class _DiscountsLifesrtyle extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 30, bottom: 8),
              child: SizedBox(
                child: Text(
                  'Descuentos',
                  style: TextStyles.h4Title.copyWith(color: Colors.black),
                ),
              )),
          Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 0, bottom: 8),
              child: SizedBox(
                child: Text(
                  'Productos eclusivos para nuestros socios',
                  style: TextStyles.middleSubtext.copyWith(color: Colors.black),
                ),
              )),
          Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(left: 3),
              margin: const EdgeInsets.only(top: 0, bottom: 8),
              child: SizedBox(
                child: TextButton(
                  style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      alignment: Alignment.centerLeft),
                  onPressed: () {
                    Navigator.of(context).push(createRoute(DiscountsScreen()));
                  },
                  child: Text(
                    'Ver todos',
                    style: TextStyles.normalTextUnderline
                        .copyWith(color: Colors.black),
                  ),
                ),
              )),
          Card(
              color: Colors.transparent,
              elevation: 0,
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: Theme.of(context).colorScheme.outline,
                ),
                borderRadius: const BorderRadius.all(Radius.circular(0)),
              ),
              margin: const EdgeInsets.only(top: 10, bottom: 8),
              child: Column(
                children: [
                  Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 0, vertical: 0),
                      child: ProductServicesWidget()),
                ],
              )),
          SizedBox(height: 200)
        ],
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
