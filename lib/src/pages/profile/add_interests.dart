import 'package:flutter/material.dart';

class AddInterests extends StatelessWidget {
  const AddInterests({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text('go to interest profile'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
