import 'package:flutter/material.dart';

class ChangePassword extends StatelessWidget {
  const ChangePassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text('go to profile'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
