import 'package:ambar_abogados/src/pages/tabs_page.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/profile_form_provider.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({super.key});

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);
    return ChangeNotifierProvider(
        create: (_) => ProfileFormProvider(profileService.selectedProfile),
        child: _ProfileEditBody(profileService: profileService));
  }
}

class _ProfileEditBody extends StatelessWidget {
  const _ProfileEditBody({
    super.key,
    required ProfileService profileService,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
            width: double.infinity,
            child: Column(
              children: [
                Container(
                    width: double.infinity,
                    child: Container(child: CloseButtonX(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ))),
                Container(
                  alignment: Alignment.topLeft,
                  padding: const EdgeInsets.only(top: 60),
                  child: const _FormScaffold(),
                ),
              ],
            ),
          ),
        ]));
  }
}

///FORM SCAFFOLD
///
///
///

class _FormScaffold extends StatelessWidget {
  const _FormScaffold({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);
    final profileForm = Provider.of<ProfileFormProvider>(context);

    final profile = profileForm.profile;

    return SingleChildScrollView(
        child: Container(
            child: Column(
      children: [
        Stack(
          children: [
            SizedBox(
              height: 100,
            ),
            ProfileAvatar(
                showname: false,
                profileService: profileService,
                datageneral: profile,
                urlimage: profileService.selectedProfile.profilePicture),
            _ProfileForm(),
            SizedBox(
              height: 100,
            ),
            IconButton(
              iconSize: 20,
              color: Colors.black,
              padding: const EdgeInsets.all(0),
              icon: Icon(Icons.camera_alt_outlined),
              onPressed: () async {
                final picker = ImagePicker();
                final XFile? pickedFile = await picker.pickImage(
                    source: ImageSource.gallery, imageQuality: 80);

                if (pickedFile == null) {
                  print('no seleccionó nada');
                  return;
                }

                print('tenemos imagen ${pickedFile.path}');

                profileService.updateSeletedProfileImage(pickedFile.path);

                final String? imageUrl = await profileService.uploadAvatar();

                if (imageUrl != null)
                  profileForm.profile.profilePicture = imageUrl;

                // if (imageUrl != null)
                //   profileService.selectedProfile.profilePicture = imageUrl;

                await profileService.saveOrUpdateProfile(profileForm.profile);
              },
            ),
            //),
          ],
        )
      ],
    )));
  }
}

/// PROFILE FORM
///
///
///
///
class _ProfileForm extends StatelessWidget {
  const _ProfileForm({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final profileForm = Provider.of<ProfileFormProvider>(context);
    final profileService = Provider.of<ProfileService>(context);
    final profile = profileForm.profile;
    late bool itsSaved = profileService.isSaved;
    late bool isSaving = profileService.isSaving;

    final dateFormatter = DateFormat('dd/MM/yyyy');

    String finalBirthDate =
        dateFormatter.format(profile.dateOfBirth as DateTime);

    return Container(
        width: double.infinity,
        padding: const EdgeInsets.only(top: 130),
        decoration: _formBoxDecoration(),
        child: Form(
            key: profileForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(children: [
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('Nombre')),
                    TextFormField(
                      validator: (value) => value == null || value.length < 1
                          ? 'El campo es obligatorio'
                          : null,
                      initialValue: profile.firstName,
                      onChanged: (value) => profile.firstName = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'nombre', labelText: 'nombre'),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('Apellido')),
                    TextFormField(
                      validator: (value) => value == null || value.length < 1
                          ? 'El campo es obligatorio'
                          : null,
                      initialValue: profile.lastName,
                      onChanged: (value) => profile.lastName = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'Apellido', labelText: 'Apellido'),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('E-mail')),
                    TextFormField(
                      validator: (value) => value == null || value.length < 1
                          ? 'El campo es obligatorio'
                          : null,
                      initialValue: profile.email,
                      onChanged: (value) => profile.email = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'E-mail', labelText: 'E-mail'),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('Teléfono')),
                    TextFormField(
                      // validator: (value) =>
                      //     value == null ? 'El campo es obligatorio' : null,
                      initialValue: profile.phone,
                      onChanged: (value) => profile.phone = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'Teléfono', labelText: 'Teléfono'),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('Fecha de Nacimiento')),
                    TextFormField(
                      validator: (value) => value == null || value.length < 1
                          ? 'El campo es obligatorio'
                          : null,
                      initialValue: finalBirthDate,
                      onChanged: (value) => finalBirthDate = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'Fecha de Nacimiento',
                              labelText: 'Fecha de Nacimiento'),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 35,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text('Mi dirección',
                      style:
                          TextStyles.bigSubtext.copyWith(color: Colors.black))),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('Calle')),
                    TextFormField(
                      // validator: (value) =>
                      //     value == null ? 'El campo es obligatorio' : null,
                      initialValue: profile.street,
                      onChanged: (value) => profile.street = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'Calle', labelText: 'Calle'),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 4,
                    child: Container(
                      width: double.infinity,
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.only(bottom: 10),
                              alignment: Alignment.topLeft,
                              child: Text('Código Postal')),
                          TextFormField(
                            validator: (value) => value == null
                                ? 'El campo es obligatorio'
                                : null,
                            initialValue: profile.zipCode,
                            onChanged: (value) => profile.zipCode = value,
                            inputFormatters: [],
                            decoration:
                                InputDecorationsProfile.profileInputDecoration(
                                    hintText: 'Código Postal',
                                    labelText: 'Código Postal'),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 20.0,
                  ),
                  Flexible(
                    flex: 7,
                    child: Container(
                      width: double.infinity,
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.only(bottom: 10),
                              alignment: Alignment.topLeft,
                              child: Text('Ciudad')),
                          TextFormField(
                            // validator: (value) => value == null
                            //     ? 'El campo es obligatorio'
                            //     : null,
                            initialValue: profile.city,
                            onChanged: (value) => profile.city = value,
                            inputFormatters: [],
                            decoration:
                                InputDecorationsProfile.profileInputDecoration(
                                    hintText: 'Ciudad', labelText: 'Ciudad'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.topLeft,
                        child: Text('Provincia')),
                    TextFormField(
                      validator: (value) =>
                          value == null ? 'El campo es obligatorio' : null,
                      initialValue: profile.province,
                      onChanged: (value) => profile.province = value,
                      inputFormatters: [],
                      decoration:
                          InputDecorationsProfile.profileInputDecoration(
                              hintText: 'Provincia', labelText: 'Provincia'),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              _FormInputSelect(
                inputLabel: 'País',
                initialValue: profile.country as String,
                req: false,
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.black,
                    minimumSize: const Size.fromHeight(40),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  onPressed: () {
                    if (!profileForm.isValidForm()) return;
                    profileService.saveOrUpdateProfile(profileForm.profile);

                    itsSaved ? showAlert(context) : null;
                  },
                  child: isSaving
                      ? const Text(
                          'Espere...',
                          style: TextStyle(fontSize: 18),
                        )
                      : const Text(
                          'Guardar',
                          style: TextStyle(fontSize: 18),
                        ),
                ),
              ),
              const SizedBox(
                height: 100,
              ),
            ])));
  }

  Future<String?> showAlert(BuildContext context) {
    return showDialog<String>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Container(
          height: 30,
          child: Center(child: const Text('Perfil actualizado')),
        ),
        content: Container(
          height: 30,
          child: Center(child: const Text('Los cambios han sido efectuados')),
        ),
        actions: <Widget>[
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.black,
                minimumSize: const Size.fromHeight(40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ),
        ],
      ),
    );
  }

  BoxDecoration _formBoxDecoration() => const BoxDecoration();
}

// /// FORM INPUT SINGLE
// ///
// ///
// ///
// ///
// ///
// class _FormInput extends StatelessWidget {
//   _FormInput({
//     super.key,
//     required this.inputLabel,
//     required this.initialValue,
//     required this.req,
//   });
//   final String inputLabel;
//   final bool req;
//   late String initialValue;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: double.infinity,
//       child: Column(
//         children: [
//           Container(
//               margin: EdgeInsets.only(bottom: 10),
//               alignment: Alignment.topLeft,
//               child: Text(inputLabel)),
//           TextFormField(
//             validator: (value) =>
//                 req == true && value == null ? 'El campo es obligatorio' : null,
//             initialValue: initialValue,
//             onChanged: (value) => initialValue = value,
//             inputFormatters: [],
//             decoration: InputDecorationsProfile.profileInputDecoration(
//                 hintText: inputLabel, labelText: inputLabel),
//           ),
//         ],
//       ),
//     );
//   }
// }

/// FORM INPUT SELECT
///
///
///
///
///

List<String> list = <String>['País', 'Two', 'Three', 'Four'];

class _FormInputSelect extends StatefulWidget {
  _FormInputSelect({
    super.key,
    required this.inputLabel,
    required this.initialValue,
    required this.req,
  });
  final String inputLabel;
  late String initialValue;
  final bool req;
  @override
  State<_FormInputSelect> createState() => _FormInputSelectState();
}

class _FormInputSelectState extends State<_FormInputSelect> {
  String dropdownValue = list.first;

  String inputLabel = 'País';
  String initialValue = 'Congo';

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  color: ColorConstants.inputBackgroundProf),
              margin: const EdgeInsets.only(bottom: 10),
              alignment: Alignment.topLeft,
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  dropdownColor: Colors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  isExpanded: true,
                  hint: Text('Seleccione el país'),
                  value: dropdownValue,
                  icon: const Icon(Icons.expand_more),
                  elevation: 2,
                  style: const TextStyle(fontSize: 16, color: Colors.black54),
                  onChanged: (String? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      dropdownValue = value!;
                    });
                  },
                  items: list.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              )),
        ],
      ),
    );
  }
}
