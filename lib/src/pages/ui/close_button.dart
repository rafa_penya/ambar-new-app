import 'package:flutter/material.dart';

class CloseButtonX extends StatelessWidget {
  const CloseButtonX({super.key, required Null Function() this.onPressed});
  final Null Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Expanded(
        flex: 10,
        child: Container(
          alignment: Alignment.centerLeft,
          child: Ink(
            width: 40,
            height: 40,
            decoration: const ShapeDecoration(
              color: Colors.black,
              shape: CircleBorder(),
            ),
            child: IconButton(
              icon: const Icon(Icons.close),
              color: Colors.white,
              onPressed: onPressed,
            ),
          ),
        ),
      ),
    );
  }
}
