import 'package:flutter/material.dart';

class TitleSliver extends StatelessWidget {
  const TitleSliver({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
      Container(
          alignment: Alignment.bottomLeft,
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ElevatedButton(
                style: OutlinedButton.styleFrom(
                    minimumSize: Size(131, 44),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    backgroundColor: Color.fromRGBO(0, 0, 0, 1)),
                child: Text('Disponibilidad'),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.star_border_outlined),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.calendar_today),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.notifications),
                onPressed: () {},
              ),
            ],
          )),
      // Stack(
      //   children: [Container(child: Text('Listado'))],
      // )
    ]);
  }
}
