import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/services/auth_service.dart';
import 'package:ambar_abogados/src/widgets/colors_constants.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SideBarNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);

    return Drawer(
      backgroundColor: Color.fromRGBO(236, 237, 239, 1),
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          // const DrawerHeader(
          //     decoration: BoxDecoration(
          //       color: Colors.blue,
          //     ),
          //     child: Text('data')),
          SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(size: 18, Icons.close, color: Colors.black),
                  style: ElevatedButton.styleFrom(
                    shape: CircleBorder(), elevation: 0,
                    padding: EdgeInsets.all(3),
                    backgroundColor: Colors.white, // <-- Button color
                    foregroundColor: Colors.red, // <-- Splash color
                  ),
                ),
              ),
            ],
          ),

          Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 15, bottom: 20, top: 10),
                child: Image(
                  alignment: Alignment.centerLeft,
                  height: 28,
                  image: AssetImage('lib/assets/images/logo-ambar.png'),
                ),
              ),
            ],
          ),

          ListTile(
            minLeadingWidth: 10,
            leading: Icon(Icons.access_alarm),
            title: const Text('Disponibilidad', style: TextStyles.menuTitle),
            onTap: () {},
          ),
          ListTile(
            minLeadingWidth: 10,
            leading: Icon(Icons.access_alarm),
            title: const Text('Agenda de eventos', style: TextStyles.menuTitle),
            onTap: () {
              Navigator.pushNamed(context, 'agenda');
            },
          ),
          ListTile(
            minLeadingWidth: 10,
            leading: Icon(Icons.access_alarm),
            title: const Text('Notificaciones', style: TextStyles.menuTitle),
            onTap: () {
              Navigator.of(context).push(createRoute(NotificationsScreen()));
            },
          ),

          ListTile(
            minLeadingWidth: 10,
            leading: Icon(Icons.exit_to_app),
            title: const Text('Logout', style: TextStyles.menuTitle),
            onTap: () {
              authService.logout();
              Navigator.pushReplacementNamed(context, 'login');
            },
          ),
        ],
      ),
    );
  }
}
