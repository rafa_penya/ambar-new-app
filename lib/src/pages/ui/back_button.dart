import 'package:flutter/material.dart';

class BackerButton extends StatelessWidget {
  final String urlBack;
  const BackerButton({super.key, required this.urlBack});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Expanded(
        flex: 10,
        child: Container(
          alignment: Alignment.centerLeft,
          child: Ink(
            width: 40,
            height: 40,
            decoration: const ShapeDecoration(
              color: Colors.black,
              shape: CircleBorder(),
            ),
            child: IconButton(
              icon: const Icon(Icons.subdirectory_arrow_left_rounded),
              color: Colors.white,
              onPressed: () =>
                  {Navigator.pushReplacementNamed(context, urlBack)},
            ),
          ),
        ),
      ),
    );
  }
}
