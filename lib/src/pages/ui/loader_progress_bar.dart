import 'package:flutter/material.dart';

class LoaderProgresBar extends StatelessWidget {
  const LoaderProgresBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          child: LinearProgressIndicator(
              minHeight: 5,
              backgroundColor: Color.fromRGBO(132, 147, 241, 0.498),
              valueColor: AlwaysStoppedAnimation(
                Color.fromRGBO(27, 122, 237, 0.498),
              )),
        )
      ],
    );

    // Container(
    //   height: 10,
    //   child: const LinearProgressIndicator(
    //     minHeight: 5,
    //     backgroundColor: Color.fromRGBO(0, 0, 0, 0.5),
    //     color: Color.fromRGBO(0, 0, 0, 0.5),
    //   ),
    // );
  }
}
