import 'package:ambar_abogados/src/models/referral.dart';
import 'package:ambar_abogados/src/pages/drawer/referral/referral_screen.dart';

import 'package:ambar_abogados/src/services/referral_service.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:provider/provider.dart';

class SelectDrop extends StatefulWidget {
  SelectDrop({
    super.key,
  });

  @override
  State<SelectDrop> createState() => _SelectDropState();
}

class _SelectDropState extends State<SelectDrop> {
  dynamic selectedValue;

  @override
  Widget build(BuildContext context) {
    final referralService = Provider.of<ReferralService>(context);

    return Card(
        child: FutureBuilder<List<ReferralRelations>>(
            future: referralService.loadReferralRelationship(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var data = snapshot.data!;

                return Center(
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton2(
                            isExpanded: true,
                            buttonStyleData: ButtonStyleData(
                              padding: const EdgeInsets.only(
                                  top: 8, bottom: 8, left: 6, right: 12),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: ColorConstants.inputBackgroundProf,
                              ),
                              elevation: 0,
                            ),

                            // icon: const Icon(Icons.keyboard_arrow_down),
                            hint: Text('Select Item'),
                            items: data
                                .map((item) => DropdownMenuItem<dynamic>(
                                      value: item.id,
                                      child: Text(
                                        item.displayValue,
                                      ),
                                    ))
                                .toList(),
                            value: selectedValue,
                            onChanged: (value) {
                              setState(() {
                                selectedValue = value;

                                ReferralScaffold.of(context)?.getAvailState =
                                    selectedValue;
                              });
                            })));
              } else {
                return const CircularProgressIndicator();
              }
            }));
  }
}
