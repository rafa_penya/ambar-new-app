import 'package:flutter/material.dart';

class ShowAlert extends StatelessWidget {
  const ShowAlert({super.key});

  void displayDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return const AlertDialog(
              elevation: 20,
              title: Text('titulo'),
              content: Text('contenido tal'));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('aleret button')),
      floatingActionButton:
          FloatingActionButton(child: Icon(Icons.close), onPressed: () {}),
    );
  }
}
