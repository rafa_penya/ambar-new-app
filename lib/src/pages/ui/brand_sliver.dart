import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';

class BrandSliver extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
      Container(
          alignment: Alignment.bottomRight,
          margin: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 8),
                    child: Image(
                      alignment: Alignment.centerLeft,
                      height: 23,
                      image: AssetImage('lib/assets/images/logo-ambar.png'),
                    ),
                  ),
                ],
              ),
              // Row(
              //   children: [
              //     ElevatedButton(
              //       onPressed: () {},
              //       child: Icon(
              //           size: 18, Icons.notifications, color: Colors.black),
              //       style: ElevatedButton.styleFrom(
              //         shape: CircleBorder(), elevation: 0,
              //         //  padding: EdgeInsets.all(3),
              //         backgroundColor:
              //             ColorConstants.inputBackground, // <-- Button color
              //         foregroundColor: Colors.red, // <-- Splash color
              //       ),
              //     ),
              //   ],
              // ),
            ],
          )),
      // Stack(
      //   children: [Container(child: Text('Listado'))],
      // )
    ]);
  }
}
