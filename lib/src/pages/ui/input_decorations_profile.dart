import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';

class InputDecorationsProfile {
  static InputDecoration profileInputDecoration(
      {required String hintText,
      required String labelText,
      IconData? prefixIcon,
      IconData? icon}) {
    return InputDecoration(
        icon: icon != null ? Icon(icon, color: Colors.deepPurple) : null,
        filled: true,
        fillColor: ColorConstants.inputBackgroundProf,
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide(
                color: ColorConstants.inputBackgroundProf, width: 0)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            borderSide: BorderSide(
                color: ColorConstants.inputBackgroundProf, width: 0)),
        hintText: hintText,
        labelStyle: const TextStyle(color: Colors.grey),
        prefixIcon: prefixIcon != null
            ? Icon(prefixIcon, color: Colors.deepPurple)
            : null);
  }
}
