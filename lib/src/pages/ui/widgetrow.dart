import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';

class WidgetRowProfile extends StatelessWidget {
  const WidgetRowProfile({super.key, required this.profData, this.labelData});

  final String? profData;
  final String? labelData;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            padding: const EdgeInsets.only(bottom: 5),
            child: Text(
              '$labelData',
              style: TextStyles.smallSubtext.copyWith(color: Colors.grey),
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            child: Text(
              '$profData',
              style: TextStyles.middleSubtext.copyWith(color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }
}
