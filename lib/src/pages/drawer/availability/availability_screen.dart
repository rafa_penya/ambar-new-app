import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/providers/profile_form_provider.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter/material.dart';

import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AvailabilityScreen extends StatefulWidget {
  // AvailabilityScreen({super.key});

  @override
  State<AvailabilityScreen> createState() => _AvailabilityScreenState();
}

class _AvailabilityScreenState extends State<AvailabilityScreen> {
  @override
  void initState() {
    super.initState();

    initializeDateFormatting('es_ES', null);
  }

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);
    final authService = Provider.of<AuthService>(context);

    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 70),
                  width: double.infinity,
                  child: Column(
                    children: [
                      Container(
                          width: double.infinity,
                          child: Container(child: CloseButtonX(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ))),
                    ],
                  ),
                ),
                AvailabilityPageBuild(profileService: profileService)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/// AVAILABILITY BUILD
///
///
///
///
///
class AvailabilityPageBuild extends StatefulWidget {
  AvailabilityPageBuild({
    super.key,
    required this.profileService,
  });

  final ProfileService profileService;

  @override
  State<AvailabilityPageBuild> createState() =>
      AvailabilityPageBuildState(profileService: profileService);

  static AvailabilityPageBuildState? of(BuildContext context) =>
      context.findAncestorStateOfType<AvailabilityPageBuildState>();
}

class AvailabilityPageBuildState extends State<AvailabilityPageBuild> {
  //seteo variable
  String? _getAvailState;

  AvailabilityPageBuildState({required this.profileService});
  final ProfileService profileService;

  set getAvailState(String? value) => setState(() => _getAvailState = value);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50),
      child: Column(
        children: [
          Container(
              alignment: Alignment.topLeft,
              child: Text(
                'Tu disponibilidad',
                style: TextStyles.bigRobotoTitle.copyWith(),
              )),
          Container(
              margin: EdgeInsets.only(top: 20),
              alignment: Alignment.topLeft,
              child: Text(
                'Gestiona tu tiempo con total flexibilidad.',
                style: TextStyles.middleSubtext.copyWith(fontSize: 17),
              )),
          Container(
              margin: EdgeInsets.only(top: 20),
              alignment: Alignment.topLeft,
              child: Text(
                'Selecciona un rango',
                style: TextStyles.middleSubtext.copyWith(fontSize: 13),
              )),
          Container(
              margin: EdgeInsets.only(top: 17),
              alignment: Alignment.topLeft,
              child: RangeSelector()),
          Container(
              margin: EdgeInsets.only(top: 57),
              alignment: Alignment.topLeft,
              child: Text(
                'No disponibilidad',
                style: TextStyles.bigRobotoTitle.copyWith(),
              )),
          Container(
              margin: EdgeInsets.only(top: 17),
              alignment: Alignment.topLeft,
              child: Text(
                'Si por cualquier razón no quieres recibir oportunidades de proyecto pausa tu disponibilidad para colaborar. No tendrás ningún tipo de penalización.',
                style: TextStyles.middleSubtext.copyWith(fontSize: 13),
              )),
          Container(
              margin: EdgeInsets.only(top: 17),
              alignment: Alignment.topLeft,
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  foregroundColor: Colors.black,
                  minimumSize: Size(150, 35),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  side: BorderSide(width: 1, color: Colors.black),
                ),
                onPressed: () {
                  Navigator.of(context).push(createRoute(ConfigAvailability()));
                },
                child: Text('Configurar'),
              )),
          SendingFormEditProfile(
            egetAvailState: _getAvailState as String?,
            profileService: profileService,
          ),
        ],
      ),
    );
  }
}

class SendingFormEditProfile extends StatelessWidget {
  const SendingFormEditProfile(
      {super.key,
      required this.egetAvailState,
      required ProfileService profileService});

  final String? egetAvailState;

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);
    return ChangeNotifierProvider(
        create: (_) => ProfileFormProvider(
              profileService.selectedProfile,
            ),
        child: _FormSending(
          profileService: profileService,
          egetAvailState: egetAvailState,
        ));
  }
}

class _FormSending extends StatelessWidget {
  const _FormSending({
    super.key,
    // required this.profileForm,
    required this.profileService,
    required this.egetAvailState,
  });

  // final ProfileFormProvider profileForm;
  final ProfileService profileService;
  final String? egetAvailState;

  @override
  Widget build(BuildContext context) {
    late bool itsSaved = profileService.isSaved;
    late bool isSaving = profileService.isSaving;
    final profileForm = Provider.of<ProfileFormProvider>(context);

    return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 17),
        alignment: Alignment.topLeft,
        child: OutlinedButton(
          style: OutlinedButton.styleFrom(
              minimumSize: const Size.fromHeight(30),
              fixedSize: Size(double.infinity, 35),
              foregroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              side: BorderSide(width: 1, color: Colors.black),
              backgroundColor: Colors.black),
          onPressed: () async {
            await profileService.updateavailability(
                profileForm.profile, egetAvailState);
            itsSaved ? showAlert(context) : null;
          },
          child: Text('Guardar'),
        ));
  }

  /// SHOW ALERT
  Future<String?> showAlert(BuildContext context) {
    return showDialog<String>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Container(
          height: 30,
          child: Center(child: const Text('Perfil actualizado')),
        ),
        content: Container(
          height: 30,
          child: Center(child: const Text('Los cambios han sido efectuados')),
        ),
        actions: <Widget>[
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.black,
                minimumSize: const Size.fromHeight(40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ),
        ],
      ),
    );
  }
}

///MENU MODEL
///
///
///
///
///

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
