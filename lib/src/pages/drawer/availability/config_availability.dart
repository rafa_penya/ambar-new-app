import 'package:ambar_abogados/src/models/profile.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/profile_form_provider.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ConfigAvailability extends StatefulWidget {
  const ConfigAvailability({super.key});
  @override
  State<ConfigAvailability> createState() => _ConfigAvailabilityState();
}

class _ConfigAvailabilityState extends State<ConfigAvailability> {
  @override
  Widget build(BuildContext context) {
    @override
    void initState() {
      super.initState();

      initializeDateFormatting('es_ES', null);
    }

    final profileService = Provider.of<ProfileService>(context);
    return ChangeNotifierProvider(
        create: (_) => ProfileFormProvider(profileService.selectedProfile),
        child: _ConfigAvailabilityBody(profileService: profileService));
  }
}

/// CONFIG AVAILABILITY BODY
///
///
///
///
///
///
class _ConfigAvailabilityBody extends StatelessWidget {
  const _ConfigAvailabilityBody({
    super.key,
    required ProfileService profileService,
  });

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);
    final authService = Provider.of<AuthService>(context);

    return Scaffold(
        body: SingleChildScrollView(
      child: ListView(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      child: Container(child: CloseButtonX(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ))),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.only(top: 60),
                    child: const _FormCAvScaffold(),
                  ),
                ],
              ),
            ),
          ]),
    ));
  }
}

/// FORM CONFIG AVAILAVILITY SCAFFOLD
///
///
///
///
///
class _FormCAvScaffold extends StatefulWidget {
  const _FormCAvScaffold({super.key});

  @override
  State<_FormCAvScaffold> createState() => _FormCAvScaffoldState();
}

class _FormCAvScaffoldState extends State<_FormCAvScaffold> {
  String? _dateinputStart;
  set dateinputStarta(String? value) => setState(() => _dateinputStart = value);
  String? _dateinputEnd;
  set dateinputEnda(String? value) => setState(() => _dateinputEnd = value);

  @override
  Widget build(BuildContext context) {
    final profileForm = Provider.of<ProfileFormProvider>(context);
    final profileService = Provider.of<ProfileService>(context);
    final profile = profileForm.profile;
    late bool itsSaved = profileService.isSaved;
    late bool isSaving = profileService.isSaving;

    TextEditingController dateinputStart = TextEditingController();
    TextEditingController dateinputEnd = TextEditingController();

    @override
    void initState() {
      super.initState();
      dateinputStart.text = "";
      dateinputEnd.text = "";
    }

    return Container(
      margin: const EdgeInsets.only(top: 0),
      child: Column(
        children: [
          Container(
              alignment: Alignment.topLeft,
              child: Text(
                'Tu disponibilidad',
                style: TextStyles.bigRobotoTitle.copyWith(),
              )),
          Container(
              margin: const EdgeInsets.only(top: 20),
              alignment: Alignment.topLeft,
              child: Text(
                'Indica a continuación el período de tiempo en el que no estarás disponible para nuevas colaboraciones.',
                style: TextStyles.middleSubtext.copyWith(fontSize: 17),
              )),
          Container(
              margin: const EdgeInsets.only(top: 40),
              alignment: Alignment.topLeft,
              child: Text(
                'Fecha de inicio y fin',
                style: TextStyles.middleSubtext.copyWith(fontSize: 13),
              )),
          Form(
            key: profileForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                _InputDateFieldStart(
                  numfield: 1,
                  profile: profile,
                  placeholder: 'Mi place 1',
                ),
                _InputDateFieldEnd(
                  numfield: 1,
                  profile: profile,
                  placeholder: 'Mi place 1',
                ),
                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 27),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        minimumSize: const Size.fromHeight(40),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      onPressed: () {
                        if (!profileForm.isValidForm()) return;

                        DateTime dtS = DateTime.parse(_dateinputStart!);
                        DateTime dtE = DateTime.parse(_dateinputEnd!);

                        if (dtS.compareTo(dtE) >= 0) {
                          showAlert(context, 'Error en la fechas',
                              'La fecha de fin no puede ser anterior a la fecha de inicio');
                          return;
                        }

                        profileService.saveAvailabilityConfig(
                            profileForm.profile,
                            _dateinputStart,
                            _dateinputEnd);

                        itsSaved
                            ? showAlert(context, 'Disponibilidad actualizada',
                                'Ha establecido el tiempo no disponible.')
                            : null;
                      },
                      child: const Text('Activar'),
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 8),
                    child: TextButton(
                      style: TextButton.styleFrom(
                        foregroundColor: Colors.black,
                        backgroundColor: Colors.transparent,
                        minimumSize: const Size.fromHeight(40),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: const Text('Cancelar'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  static _FormCAvScaffoldState? of(BuildContext context) =>
      context.findAncestorStateOfType<_FormCAvScaffoldState>();

  /// SHOW ALERT
  Future<String?> showAlert(BuildContext context, title, text) {
    return showDialog<String>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Container(
          height: 30,
          child: Center(child: Text(title)),
        ),
        content: Container(
          height: 60,
          child: Center(child: Text(text)),
        ),
        actions: <Widget>[
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.black,
                minimumSize: const Size.fromHeight(40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ),
        ],
      ),
    );
  }
}

/// INPUT FIELD START
///
///
///
///
///
///
class _InputDateFieldStart extends StatefulWidget {
  const _InputDateFieldStart({
    super.key,
    required this.profile,
    required this.numfield,
    required this.placeholder,
  });

  final Profile profile;
  final int numfield;
  final String placeholder;

  @override
  State<_InputDateFieldStart> createState() => _InputDateFieldStartState();
}

class _InputDateFieldStartState extends State<_InputDateFieldStart> {
  TextEditingController dateinputStart = TextEditingController();

  @override
  void initState() {
    super.initState();
    dateinputStart.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      alignment: Alignment.topLeft,
      child: Expanded(
        flex: 5,
        child: TextFormField(
          controller: dateinputStart,
          decoration: InputDecoration(
            icon: const Icon(Icons.calendar_today),
            labelText: widget.placeholder,
          ),
          readOnly: true,
          validator: (value) => value == null || value.length < 1
              ? 'El campo es obligatorio'
              : null,
          onTap: () async {
            DateTime? date = DateTime(1900);
            FocusScope.of(context).requestFocus(new FocusNode());
            date = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(1900),
                lastDate: DateTime(2100));
            dateinputStart.text = DateFormat('yyyy-MM-dd').format(date!);

            setState(() {
              _FormCAvScaffoldState.of(context)?.dateinputStarta =
                  dateinputStart.text;
            });
          },
        ),
      ),
    );
  }
}

/// INPUT FIELD END
///
///
///
///
///
///
class _InputDateFieldEnd extends StatefulWidget {
  const _InputDateFieldEnd({
    super.key,
    required this.profile,
    required this.numfield,
    required this.placeholder,
  });

  final Profile profile;
  final int numfield;
  final String placeholder;

  @override
  State<_InputDateFieldEnd> createState() => _InputDateFieldEndState();
}

class _InputDateFieldEndState extends State<_InputDateFieldEnd> {
  TextEditingController dateinputEnd = TextEditingController();

  @override
  void initState() {
    super.initState();
    dateinputEnd.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      alignment: Alignment.topLeft,
      child: Expanded(
        flex: 5,
        child: TextFormField(
          controller: dateinputEnd,
          decoration: InputDecoration(
            icon: const Icon(Icons.calendar_today),
            labelText: widget.placeholder,
          ),
          readOnly: true,
          validator: (value) => value == null || value.length < 1
              ? 'El campo es obligatorio'
              : null,
          onTap: () async {
            DateTime? date = DateTime(1900);
            FocusScope.of(context).requestFocus(new FocusNode());
            date = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(1900),
                lastDate: DateTime(2100));

            dateinputEnd.text = DateFormat('yyyy-MM-dd').format(date!);

            setState(() {
              _FormCAvScaffoldState.of(context)?.dateinputEnda =
                  dateinputEnd.text;
            });
          },
        ),
      ),
    );
  }
}
