import 'dart:async';
import 'package:ambar_abogados/src/models/awards.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/awards_form_provider.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:regexed_validator/regexed_validator.dart';

class AwardsScreen extends StatefulWidget {
  const AwardsScreen({super.key});
  @override
  State<AwardsScreen> createState() => _AwardsScreenState();
}

class _AwardsScreenState extends State<AwardsScreen> {
  @override
  Widget build(BuildContext context) {
    final awardsService = Provider.of<AwardsService>(context);
    return ChangeNotifierProvider(
        create: (_) => AwardsFormProvider(awardsService.selectedAward),
        child: _AwardsScreenBody(awardsService: awardsService));
  }
}

/// AWARDS BODY
///
///
class _AwardsScreenBody extends StatelessWidget {
  const _AwardsScreenBody({
    super.key,
    required this.awardsService,
  });

  final AwardsService awardsService;

  void initState() {
    initializeDateFormatting('es_ES', null);
  }

  @override
  Widget build(BuildContext context) {
    final awardsService = Provider.of<AwardsService>(context);
    final authService = Provider.of<AuthService>(context);

    return Scaffold(
        body: SingleChildScrollView(
      child: ListView(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      child: Container(child: CloseButtonX(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ))),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.only(top: 60),
                    child: const AwardsScaffold(),
                  ),
                ],
              ),
            ),
          ]),
    ));
  }
}

/// FORM AWARDS SCAFFOLD
///
/// In this place we put Awards list
///
class AwardsScaffold extends StatefulWidget {
  const AwardsScaffold({super.key});
  @override
  State<AwardsScaffold> createState() => AwardsScaffoldState();

  static AwardsScaffoldState? of(BuildContext context) =>
      context.findAncestorStateOfType<AwardsScaffoldState>();
}

class AwardsScaffoldState extends State<AwardsScaffold> {
  int? _getAvailState;
  set getAvailState(int? value) => setState(() => _getAvailState = value);
  @override
  Widget build(BuildContext context) {
    final awardsForm = Provider.of<AwardsFormProvider>(context);
    final awardsService = Provider.of<AwardsService>(context);
    final awards = awardsForm.awards;
    late bool itsSaved = awardsService.isSaved;
    late bool isSaving = awardsService.isSaving;

    ///FUTURE

    Awards? dropdownvalue;

    return Container(
      margin: const EdgeInsets.only(top: 0),
      child: Column(
        children: [
          FutureBuilder<List<Awards>>(
              future: awardsService.loadAwards(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<Awards>> snapshot) {
                if (snapshot.hasData) {
                  return Column(mainAxisSize: MainAxisSize.max, children: [
                    Container(
                      height: 310 * snapshot.data!.length.toDouble(),
                      child: Column(
                        children: <Widget>[
                          Container(
                              alignment: Alignment.topLeft,
                              margin: const EdgeInsets.only(bottom: 25),
                              child: Text(
                                'Referral',
                                style: TextStyles.h1Title.copyWith(),
                              )),
                          Container(
                              margin: const EdgeInsets.only(top: 40),
                              padding: const EdgeInsets.all(10),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Adelante, invita a quien creas que debería ser socio:',
                                style: TextStyles.middleSubtext
                                    .copyWith(fontSize: 18),
                              )),
                        ],
                      ),
                    )
                  ]);
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return LoaderProgresBar();
              }),

          /// FORM SEND DATA
          ///
          ///
          Container(
            padding: const EdgeInsets.all(10),
            child: Form(
              key: awardsForm.formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  ///---------FORM INPUT TEXTFIELDS---------------------------------------
                  ///
                  ///
                  ///
                  // Container(
                  //     margin: const EdgeInsets.only(top: 10, bottom: 12),
                  //     alignment: Alignment.topLeft,
                  //     child: const Text(
                  //       'Nombre',
                  //     )),
                  // TextFormField(
                  //   validator: (value) => value == null || value.length < 1
                  //       ? 'El campo es obligatorio'
                  //       : null,
                  //   onChanged: (value) => awards.name = value,
                  //   decoration: InputDecorationsProfile.profileInputDecoration(
                  //       hintText: 'nombre', labelText: 'nombre'),
                  // ),
                  // Container(
                  //     margin: const EdgeInsets.only(top: 34, bottom: 12),
                  //     alignment: Alignment.topLeft,
                  //     child: const Text(
                  //       'Apellidos',
                  //     )),
                  // TextFormField(
                  //   validator: (value) => value == null || value.length < 1
                  //       ? 'El campo es obligatorio'
                  //       : null,
                  //   onChanged: (value) => awards.surnames = value,
                  //   decoration: InputDecorationsProfile.profileInputDecoration(
                  //       hintText: 'apellidos', labelText: 'apellidos'),
                  // ),

                  // Container(
                  //     margin: const EdgeInsets.only(top: 34, bottom: 12),
                  //     alignment: Alignment.topLeft,
                  //     child: const Text(
                  //       'E-mail',
                  //     )),
                  // TextFormField(
                  //   validator: (value) => value == null ||
                  //           value.length < 1 ||
                  //           !validator.email(value)
                  //       ? 'El campo es obligatorio y debe ser un e-mail válido'
                  //       : null,
                  //   onChanged: (value) => awards.email = value,
                  //   decoration: InputDecorationsProfile.profileInputDecoration(
                  //       hintText: 'email', labelText: 'email'),
                  // ),

                  // Container(
                  //     margin: const EdgeInsets.only(top: 34, bottom: 12),
                  //     alignment: Alignment.topLeft,
                  //     child: const Text(
                  //       'LinkedIn',
                  //     )),
                  // TextFormField(
                  //   validator: (value) => value!.length > 1
                  //       ? !validator.url(value)
                  //           ? 'El campo debe ser una URI válida'
                  //           : null
                  //       : null,
                  //   onChanged: (value) => awards.linkedin = value,
                  //   decoration: InputDecorationsProfile.profileInputDecoration(
                  //       hintText: 'https://www.linkedin.com/profile',
                  //       labelText: 'LinkedIn'),
                  // ),

                  ///--------------------------------------
                  ///  SELECT DROPDOWN
                  ///--------------------------------------

                  // Container(
                  //     margin: const EdgeInsets.only(top: 34, bottom: 12),
                  //     alignment: Alignment.topLeft,
                  //     child: const Text(
                  //       '¿Qué relación tienes con esta persona?',
                  //     )),

                  // SelectDrop(),

                  ///--------------------------------------
                  ///  END SELECT DROPDOWN
                  ///--------------------------------------
                  ///
                  // Container(
                  //     margin: const EdgeInsets.only(top: 34, bottom: 12),
                  //     alignment: Alignment.topLeft,
                  //     child: const Text(
                  //       'Comentarios',
                  //     )),
                  // TextFormField(
                  //   maxLines: 6,
                  //   onChanged: (value) => awards.comment = value,
                  //   decoration: InputDecorationsProfile.profileInputDecoration(
                  //       hintText: 'Comentarios', labelText: 'Comentarios'),
                  // ),

                  ///
                  ///----------------------------------------------------------------------
                  // Center(
                  //   child: Container(
                  //     margin: const EdgeInsets.only(top: 27),
                  //     child: ElevatedButton(
                  //       style: ElevatedButton.styleFrom(
                  //         disabledForegroundColor: Colors.white,
                  //         disabledBackgroundColor:
                  //             ColorConstants.kColorsBlueGrey400,
                  //         backgroundColor: Colors.black,
                  //         minimumSize: const Size.fromHeight(40),
                  //         shape: RoundedRectangleBorder(
                  //           borderRadius: BorderRadius.circular(30),
                  //         ),
                  //       ),
                  //       onPressed: () {
                  //         if (!awardsForm.isValidForm()) {
                  //           showAlert(context, 'Error',
                  //               'Hay campos que debe rellenar.');
                  //           return;
                  //         }

                  //         awards.partnerRelationship = _getAvailState!;

                  //         awardsService.saveReferral(
                  //           awardsForm.awards,
                  //         );
                  //         var funcionSaved =
                  //             Timer(const Duration(seconds: 1), () {
                  //           const LinearProgressIndicator();

                  //           showAlert(context, 'Persona enviada',
                  //               'Su solicitud ha sido enviada, la revisaremos en breve.');
                  //         });
                  //         itsSaved ? funcionSaved : null;
                  //       },
                  //       child: const Text('Invitar nuevo socio'),
                  //     ),
                  //   ),
                  // ),
                  const SizedBox(
                    height: 150,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// SHOW ALERT
  Future<String?> showAlert(BuildContext context, title, text) {
    return showDialog<String>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Container(
          height: 30,
          child: Center(child: Text(title)),
        ),
        content: Container(
          height: 60,
          child: Center(child: Text(text)),
        ),
        actions: <Widget>[
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.black,
                minimumSize: const Size.fromHeight(40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              onPressed: () => Navigator.pushNamed(context, 'home'),
              child: const Text('OK'),
            ),
          ),
        ],
      ),
    );
  }
}
