import 'dart:async';
import 'package:ambar_abogados/src/models/referral.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/referral_form_provider.dart';
import 'package:ambar_abogados/src/services/referral_service.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:regexed_validator/regexed_validator.dart';

class ReferralScreen extends StatefulWidget {
  const ReferralScreen({super.key});
  @override
  State<ReferralScreen> createState() => _ReferralScreenState();
}

class _ReferralScreenState extends State<ReferralScreen> {
  @override
  Widget build(BuildContext context) {
    final referralService = Provider.of<ReferralService>(context);
    return ChangeNotifierProvider(
        create: (_) => ReferralFormProvider(referralService.selectedReferral),
        child: _ReferralScreenBody(referralService: referralService));
  }
}

/// REFERRAL BODY
///
///
class _ReferralScreenBody extends StatelessWidget {
  const _ReferralScreenBody({
    super.key,
    required this.referralService,
  });

  final ReferralService referralService;

  void initState() {
    initializeDateFormatting('es_ES', null);
  }

  @override
  Widget build(BuildContext context) {
    final referralService = Provider.of<ReferralService>(context);
    final authService = Provider.of<AuthService>(context);

    return Scaffold(
        body: SingleChildScrollView(
      child: ListView(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      child: Container(child: CloseButtonX(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ))),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: const EdgeInsets.only(top: 60),
                    child: const ReferralScaffold(),
                  ),
                ],
              ),
            ),
          ]),
    ));
  }
}

/// FORM REFERRAL SCAFFOLD
///
///
///
class ReferralScaffold extends StatefulWidget {
  const ReferralScaffold({super.key});
  @override
  State<ReferralScaffold> createState() => ReferralScaffoldState();

  static ReferralScaffoldState? of(BuildContext context) =>
      context.findAncestorStateOfType<ReferralScaffoldState>();
}

class ReferralScaffoldState extends State<ReferralScaffold> {
  int? _getAvailState;
  set getAvailState(int? value) => setState(() => _getAvailState = value);
  @override
  Widget build(BuildContext context) {
    final referralForm = Provider.of<ReferralFormProvider>(context);
    final referralService = Provider.of<ReferralService>(context);
    final referral = referralForm.referral;
    late bool itsSaved = referralService.isSaved;
    late bool isSaving = referralService.isSaving;

    ///FUTURE

    ReferralRelations? dropdownvalue;

    return Container(
      margin: const EdgeInsets.only(top: 0),
      child: Column(
        children: [
          Container(
              alignment: Alignment.topLeft,
              margin: const EdgeInsets.only(bottom: 25),
              child: Text(
                'Referral',
                style: TextStyles.h1Title.copyWith(),
              )),
          Card(
            color: ColorConstants.menuBackground,
            shape: const RoundedRectangleBorder(
                side: BorderSide(color: Colors.transparent, width: 0),
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Container(
                padding: const EdgeInsets.all(25),
                alignment: Alignment.topLeft,
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(bottom: 30),
                      child: Text(
                        'Programa de referral',
                        style: TextStyles.menuTitle
                            .copyWith(color: Colors.white, fontSize: 30),
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Container(
                                  padding: const EdgeInsets.only(top: 4),
                                  height: 80,
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 25.0,
                                    height: 25.0,
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        color: Colors.white,
                                        width: 1.0,
                                        style: BorderStyle.solid,
                                      ),
                                    ),
                                    child: const Center(
                                      child: Text(
                                        '1',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                        ),
                                      ),
                                    ),
                                  ))),
                          Expanded(
                            flex: 9,
                            child: Container(
                              height: 80,
                              padding: const EdgeInsets.only(left: 10),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Rellena el formulario con los datos del abogado/a que quieres recomendar.',
                                style: TextStyles.middleSubtext.copyWith(
                                    color: Colors.white,
                                    height: 1.4,
                                    fontSize: 15),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Container(
                                  padding: const EdgeInsets.only(top: 4),
                                  height: 80,
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 25.0,
                                    height: 25.0,
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        color: Colors.white,
                                        width: 1.0,
                                        style: BorderStyle.solid,
                                      ),
                                    ),
                                    child: const Center(
                                      child: Text(
                                        '2',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.0,
                                        ),
                                      ),
                                    ),
                                  ))),
                          Expanded(
                            flex: 9,
                            child: Container(
                              height: 80,
                              padding: const EdgeInsets.only(left: 10),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Avísale que nos pondremos en contacto con él/ella.',
                                style: TextStyles.middleSubtext.copyWith(
                                    color: Colors.white,
                                    height: 1.4,
                                    fontSize: 15),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
          Container(
              margin: const EdgeInsets.only(top: 40),
              padding: const EdgeInsets.all(10),
              alignment: Alignment.topLeft,
              child: Text(
                'Adelante, invita a quien creas que debería ser socio:',
                style: TextStyles.middleSubtext.copyWith(fontSize: 18),
              )),
          Container(
            padding: const EdgeInsets.all(10),
            child: Form(
              key: referralForm.formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  ///---------FORM INPUT TEXTFIELDS---------------------------------------
                  ///
                  ///
                  ///
                  Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 12),
                      alignment: Alignment.topLeft,
                      child: const Text(
                        'Nombre',
                      )),
                  TextFormField(
                    validator: (value) => value == null || value.length < 1
                        ? 'El campo es obligatorio'
                        : null,
                    onChanged: (value) => referral.name = value,
                    decoration: InputDecorationsProfile.profileInputDecoration(
                        hintText: 'nombre', labelText: 'nombre'),
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 34, bottom: 12),
                      alignment: Alignment.topLeft,
                      child: const Text(
                        'Apellidos',
                      )),
                  TextFormField(
                    validator: (value) => value == null || value.length < 1
                        ? 'El campo es obligatorio'
                        : null,
                    onChanged: (value) => referral.surnames = value,
                    decoration: InputDecorationsProfile.profileInputDecoration(
                        hintText: 'apellidos', labelText: 'apellidos'),
                  ),

                  Container(
                      margin: const EdgeInsets.only(top: 34, bottom: 12),
                      alignment: Alignment.topLeft,
                      child: const Text(
                        'E-mail',
                      )),
                  TextFormField(
                    validator: (value) => value == null ||
                            value.length < 1 ||
                            !validator.email(value)
                        ? 'El campo es obligatorio y debe ser un e-mail válido'
                        : null,
                    onChanged: (value) => referral.email = value,
                    decoration: InputDecorationsProfile.profileInputDecoration(
                        hintText: 'email', labelText: 'email'),
                  ),

                  Container(
                      margin: const EdgeInsets.only(top: 34, bottom: 12),
                      alignment: Alignment.topLeft,
                      child: const Text(
                        'LinkedIn',
                      )),
                  TextFormField(
                    validator: (value) => value!.length > 1
                        ? !validator.url(value)
                            ? 'El campo debe ser una URI válida'
                            : null
                        : null,
                    onChanged: (value) => referral.linkedin = value,
                    decoration: InputDecorationsProfile.profileInputDecoration(
                        hintText: 'https://www.linkedin.com/profile',
                        labelText: 'LinkedIn'),
                  ),

                  ///--------------------------------------
                  ///  SELECT DROPDOWN
                  ///--------------------------------------

                  Container(
                      margin: const EdgeInsets.only(top: 34, bottom: 12),
                      alignment: Alignment.topLeft,
                      child: const Text(
                        '¿Qué relación tienes con esta persona?',
                      )),

                  SelectDrop(),

                  ///--------------------------------------
                  ///  END SELECT DROPDOWN
                  ///--------------------------------------
                  ///
                  Container(
                      margin: const EdgeInsets.only(top: 34, bottom: 12),
                      alignment: Alignment.topLeft,
                      child: const Text(
                        'Comentarios',
                      )),
                  TextFormField(
                    maxLines: 6,
                    onChanged: (value) => referral.comment = value,
                    decoration: InputDecorationsProfile.profileInputDecoration(
                        hintText: 'Comentarios', labelText: 'Comentarios'),
                  ),

                  ///
                  ///----------------------------------------------------------------------
                  Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 27),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          disabledForegroundColor: Colors.white,
                          disabledBackgroundColor:
                              ColorConstants.kColorsBlueGrey400,
                          backgroundColor: Colors.black,
                          minimumSize: const Size.fromHeight(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                        onPressed: () {
                          if (!referralForm.isValidForm()) {
                            showAlert(context, 'Error',
                                'Hay campos que debe rellenar.');
                            return;
                          }

                          referral.partnerRelationship = _getAvailState!;

                          referralService.saveReferral(
                            referralForm.referral,
                          );
                          var funcionSaved =
                              Timer(const Duration(seconds: 1), () {
                            const LinearProgressIndicator();

                            showAlert(context, 'Persona enviada',
                                'Su solicitud ha sido enviada, la revisaremos en breve.');
                          });
                          itsSaved ? funcionSaved : null;
                        },
                        child: const Text('Invitar nuevo socio'),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 150,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// SHOW ALERT
  Future<String?> showAlert(BuildContext context, title, text) {
    return showDialog<String>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Container(
          height: 30,
          child: Center(child: Text(title)),
        ),
        content: Container(
          height: 60,
          child: Center(child: Text(text)),
        ),
        actions: <Widget>[
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.black,
                minimumSize: const Size.fromHeight(40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              onPressed: () => Navigator.pushNamed(context, 'home'),
              child: const Text('OK'),
            ),
          ),
        ],
      ),
    );
  }
}
