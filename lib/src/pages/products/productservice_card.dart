import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ProductServiceCard extends StatelessWidget {
  const ProductServiceCard({super.key, required this.data});

  final ResultProdSer data;

  @override
  Widget build(BuildContext context) {
    final prodsservService = Provider.of<ProdServService>(context);

    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        decoration: _cardBorders(),
        width: double.infinity,
        child: Stack(children: [Container(child: _CardServProd(data: data))]),
      ),
    );
  }

  BoxDecoration _cardBorders() => BoxDecoration(color: Colors.transparent);
}

/// Serv and prod card
///
///
///
class _CardServProd extends StatelessWidget {
  const _CardServProd({
    super.key,
    required this.data,
  });

  final ResultProdSer data;

  @override
  Widget build(BuildContext context) {
    // final dateFormatter = DateFormat.yMMMMd('es_ES');
    // final String finalStartDate =
    // dateFormatter.format(data.startDate as DateTime);

    return Container(
        padding: EdgeInsets.all(0),
        child: Card(
            color: Colors.transparent,
            elevation: 0,
            shape: RoundedRectangleBorder(
              side: BorderSide.none,
            ),
            child: Container(
                child: Column(
              children: [
                Container(
                  height: 180,
                  width: double.infinity,
                  child: CachedNetworkImage(
                      placeholder: (context, url) => Container(
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    'lib/assets/images/loading1.gif'),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                      imageUrl: data.image,
                      imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),

                  //       fit: BoxFit.cover),
                  //  ),
                ),
                // SizedBox(height: 10),
                // Container(
                // alignment: Alignment.centerLeft,
                // child: Text(
                // finalStartDate,
                // style: TextStyles.smallSubtext.copyWith(),
                // ),
                // ),
                SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Flexible(
                    child: RichText(
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      strutStyle: StrutStyle(),
                      text: TextSpan(
                        style:
                            TextStyles.menuTitle.copyWith(color: Colors.black),
                        text: data.title,
                      ),
                    ),
                  ),
                ),
              ],
            ))));
  }
}
