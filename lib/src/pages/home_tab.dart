import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/models/profile.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/pages/ui/sidebar_navigation.dart';
import 'package:ambar_abogados/src/pages/ui/sliver_custome_header.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/services/auth_service.dart';
import 'package:ambar_abogados/src/services/profile_service.dart';
import 'package:ambar_abogados/src/services/referral_service.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:provider/provider.dart';

class HomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        endDrawer: SideBarNavigation(),
        body: Stack(
          children: <Widget>[
            CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildListDelegate([
                  ///HOME CONTENT
                  HomeGrid(),
                ]))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

///HOME GRID CREATION
class HomeGrid extends StatefulWidget {
  @override
  _HomeGridState createState() => _HomeGridState();
}

class _HomeGridState extends State<HomeGrid> {
  ScrollController controller = ScrollController();
  double scrollAnterior = 0;

  @override
  void initState() {
    controller.addListener(() {
      if (controller.offset > scrollAnterior && controller.offset > 150) {
        Provider.of<_MenuModel>(context, listen: false).mostrar =
            true; //en false si queremos que desaparezca al hacer scroll
      } else {
        Provider.of<_MenuModel>(context, listen: false).mostrar = true;
      }
      scrollAnterior = controller.offset;
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Container(
          //     margin: const EdgeInsets.only(top: 25),
          //     child: const SizedBox(
          //       child: BackerButton(urlBack: 'login'),
          //     )),
          Column(
            children: [
              Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 8),
                  child: SizedBox(
                    child: FadeInDown(
                        child: Text(
                      'EL lugar para crecer y compartir',
                      style: TextStyles.h1Title.copyWith(color: Colors.black),
                    )),
                  )),
              Container(
                margin: const EdgeInsets.only(top: 15, bottom: 40),
                child: SizedBox(
                  child: Row(
                    children: [
                      Text(
                        '“ ',
                        style: TextStyles.h1Title.copyWith(fontSize: 40),
                      ),
                      Flexible(
                          child: FadeInLeft(
                        child: Text(
                            'Para cambiar el mundo,              primero hagámoslo nosotros.',
                            style: TextStyle(fontSize: 19)),
                      )),
                      Text(
                        '”',
                        style: TextStyles.h1Title.copyWith(fontSize: 40),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 40, bottom: 8),
                  child: SizedBox(
                    child: Text(
                      'Te lo ponemos fácil para estar al día',
                      style: TextStyles.h4Title.copyWith(color: Colors.black),
                    ),
                  )),
              _YourAvailability(),
              _SendNewPartners(),
              _ProyectosHome(),
              _AwardsHome(),
              SizedBox(
                height: 200,
              )
            ],
          ),
        ],
      ),
    );
  }
}

///SEND NEW PARTNERS BLOCK
class _SendNewPartners extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    final referralService = Provider.of<ReferralService>(context);
    return Center(
      child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.outline,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          color: const Color.fromRGBO(181, 83, 50, 1),
          margin: const EdgeInsets.only(top: 40, bottom: 8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(height: 100),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                width: double.infinity,
                child: Text(
                  'REFERRAL',
                  style: TextStyles.smallSubtext
                      .copyWith(color: ColorConstants.inactiveColorMenu),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                width: double.infinity,
                child: Text(
                  'Programa de Referral',
                  style: TextStyles.h4Title.copyWith(color: Colors.white),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 40),
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                          border: Border(
                        left: BorderSide(
                          color: Colors.white,
                          width: 1,
                        ),
                      )),
                      child: Text(
                        'Suma Ambar Coins en “My Wallet” y haz crecer A-Club.',
                        style: TextStyles.middleSubtext
                            .copyWith(color: ColorConstants.whiteColour),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    side: const BorderSide(color: Colors.white, width: 1),
                    foregroundColor: Colors.white,
                    minimumSize: const Size(131, 44),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                  ),
                  child: const Text('Referral partner'),
                  onPressed: () {
                    referralService.selectedReferral = Referral(
                        name: '',
                        surnames: '',
                        email: '',
                        linkedin: '',
                        comment: '',
                        partnerRelationship: 0);
                    Navigator.of(context).push(createRoute(ReferralScreen()));
                  },
                ),
              ),
              const SizedBox(height: 50),
            ],
          )),
    );
  }
}

///YOUR AVAILABILITY BLOCK
class _YourAvailability extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    final profileService = Provider.of<ProfileService>(context);

    return Center(
      child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.outline,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          color: const Color.fromRGBO(40, 73, 67, 1),
          margin: const EdgeInsets.only(top: 40, bottom: 8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(height: 100),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                width: double.infinity,
                child: Text(
                  'DISPONIBILIDAD',
                  style: TextStyles.smallSubtext
                      .copyWith(color: ColorConstants.inactiveGreenColorMenu),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                width: double.infinity,
                child: Text(
                  'Actualiza tu disponibilidad',
                  style: TextStyles.h4Title.copyWith(color: Colors.white),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 40),
                width: double.infinity,
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                          border: Border(
                        left: BorderSide(
                          color: Colors.white,
                          width: 1,
                        ),
                      )),
                      child: Text(
                        'Para que podamos ofrecerte proyectos que encajen con tu disponibilidad, mantén los datos actualizados.',
                        style: TextStyles.middleSubtext
                            .copyWith(color: ColorConstants.whiteColour),
                      ),
                    ),
                  ],
                ),
              ),
              FutureBuilder<Profile>(
                future: profileService.getUser(),
                builder:
                    (BuildContext context, AsyncSnapshot<Profile> snapshot) {
                  if (snapshot.hasData) {
                    final datageneral = snapshot.data!;
                    return Column(
                      children: [
                        Container(
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              side: const BorderSide(
                                  color: Colors.white, width: 1),
                              foregroundColor: Colors.white,
                              minimumSize: const Size(131, 44),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                            ),
                            child: const Text('Disponibilidad'),
                            onPressed: () {
                              profileService.selectedProfile = datageneral;
                              Navigator.of(context)
                                  .push(createRoute(AvailabilityScreen()));
                            },
                          ),
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }
                  return const CircularProgressIndicator(
                    color: Colors.amber,
                  );
                },
              ),
              const SizedBox(height: 50),
            ],
          )),
    );
  }
}

///HOME PROJECTS LIST
class _ProyectosHome extends StatelessWidget {
  GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final navModel = Provider.of<NavigtionModel>(context);

    print(_scaffoldKey);
    return Center(
      child: FadeIn(
        child: Card(
            elevation: 0,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: Theme.of(context).colorScheme.outline,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(12)),
            ),
            color: const Color.fromRGBO(18, 49, 161, 1),
            margin: const EdgeInsets.only(top: 40, bottom: 8),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const SizedBox(height: 10),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  width: double.infinity,
                  child: Text(
                    'Febrero',
                    style: TextStyles.smallSubtext
                        .copyWith(color: ColorConstants.inactiveGreenColorMenu),
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 0),
                  width: double.infinity,
                  child: Text(
                    'La acción más pequeña es mejor que la intención más grande.',
                    style: TextStyles.h4Title.copyWith(color: Colors.white),
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        side: const BorderSide(color: Colors.white, width: 1),
                        foregroundColor: Colors.white,
                        minimumSize: const Size(131, 44),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                      ),
                      child: const Text('Ver proyectos'),
                      onPressed: () => navModel.currentPage = 2),
                ),
                const Positioned(
                  right: 1,
                  bottom: 1,
                  child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 55),
                      child: Image(
                        image: AssetImage('lib/assets/images/probono-big.png'),
                      )),
                ),
              ],
            )),
      ),
    );
  }
}

///AWARDS BANNER HOME
class _AwardsHome extends StatelessWidget {
  // final int index;

  //const _SendNewPartners(this.index);

  @override
  Widget build(BuildContext context) {
    final awardsService = Provider.of<AwardsService>(context);

    return Center(
      child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.outline,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          color: const Color.fromRGBO(18, 49, 161, 1),
          margin: const EdgeInsets.only(top: 40, bottom: 8),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 420,
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    image: DecorationImage(
                      image: AssetImage('lib/assets/images/premios-mobile.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 25,
                          vertical: 25,
                        ),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 255,
                              child: Text(
                                'Este premio podría ser tuyo, ¿te apuntas?',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            SvgPicture.asset(
                              height: 35,
                              'lib/assets/icons/laurel.svg',
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 0,
                        bottom: 1,
                        child: Consumer(
                          builder: (context, ref, child) {
                            //final data = ref.watch(loadAwardsState);
                            return Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 8,
                                vertical: 8,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 25),
                                    width: 255,
                                    child: Text(
                                      'Medalla de Oro 2022 del Foro de Empleo Cum Laude',
                                      style: TextStyles.h4Title
                                          .copyWith(color: Colors.white),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 25),
                                    child: OutlinedButton(
                                      style: OutlinedButton.styleFrom(
                                        side: const BorderSide(
                                            color: Colors.white, width: 1),
                                        foregroundColor: Colors.white,
                                        minimumSize: const Size(131, 44),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                      ),
                                      child: const Text('Quiero inscribirme'),
                                      onPressed: () {
                                        awardsService.selectedAward =
                                            Awards(id: 0);

                                        Navigator.of(context)
                                            .push(createRoute(AwardsScreen()));
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
