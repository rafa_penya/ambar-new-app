import 'package:ambar_abogados/src/models/models.dart';
import 'package:ambar_abogados/src/pages/events/event_card.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EventsScreen extends StatefulWidget {
  @override
  State<EventsScreen> createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  int _counter = 0;
  late ScrollController _hideButtonController;
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  var _isVisible;

  @override
  initState() {
    super.initState();
    _isVisible = true;
    _hideButtonController = ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (_isVisible == true) {
          setState(() {
            _isVisible = false;
          });
        }
      } else {
        if (_hideButtonController.position.userScrollDirection ==
            ScrollDirection.reverse) {
          if (_isVisible == false) {
            setState(() {
              _isVisible = true;
            });
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final widgetNum;
    return ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Scaffold(
        floatingActionButton: Visibility(
          visible: _isVisible,
          child: FloatingActionButton(
              backgroundColor: Colors.black,
              child: const Icon(Icons.arrow_upward),
              onPressed: () {
                _hideButtonController.animateTo(0,
                    duration:
                        const Duration(milliseconds: 500), //duration of scroll
                    curve: Curves.fastOutSlowIn //scroll type
                    );
              }),
        ),
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              controller: _hideButtonController,
              child: EventsGrid(),
            ),
          ],
        ),
      ),
    );
  }
}

///EVENTS GRID CREATION
///
///
///
///
class EventsGrid extends StatefulWidget {
  @override
  _EventsGridState createState() => _EventsGridState();
}

class _EventsGridState extends State<EventsGrid> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Column(
            children: [
              _MyEvents(),
              const SizedBox(height: 150),
            ],
          ),
        ],
      ),
    );
  }
}

///MY PROJECTS BLOCK
///
///
///
///
class _MyEvents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            width: double.infinity,
            alignment: Alignment.centerLeft,
            child: const _EventsTabs()),
      ],
    );
  }
}

///EVENTS TABS
///
///
///
///
class _EventsTabs extends StatefulWidget {
  const _EventsTabs({super.key});

  @override
  State<_EventsTabs> createState() => _EventsTabsWidgetState();
}

class _EventsTabsWidgetState extends State<_EventsTabs>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final eventsService = Provider.of<EventsService>(context);
    return Column(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(top: 85),
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      child: Container(child: CloseButtonX(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ))),
                ],
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(top: 55, bottom: 35),
              child: Text(
                'Eventos',
                style: TextStyles.h4Title.copyWith(fontSize: 34),
              ),
            ),
            Container(
              height: 30,
              margin: const EdgeInsets.only(bottom: 0),
              width: double.infinity,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey),
                ),
              ),
              child: TabBar(
                physics: const NeverScrollableScrollPhysics(),
                indicatorColor: Colors.transparent,
                indicatorWeight: 1,
                indicatorPadding:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                indicatorSize: TabBarIndicatorSize.tab,
                labelColor: Colors.black,
                labelPadding: const EdgeInsets.only(left: 2),
                unselectedLabelColor: Colors.grey,
                controller: _tabController,
                isScrollable: false,
                tabs: const <Widget>[
                  Tab(
                    child: SizedBox(
                      width: 70,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Todos",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      width: 100,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Wellbeing",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      width: 90,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Growth",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: SizedBox(
                      width: 90,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Social",
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        FutureBuilder<List<ResultEvents>>(
            future: eventsService.loadEvents(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ResultEvents>> snapshot) {
              if (snapshot.hasData) {
                return Column(mainAxisSize: MainAxisSize.max, children: [
                  Container(
                    height: 310 * snapshot.data!.length.toDouble(),
                    child: TabBarView(
                      physics: const NeverScrollableScrollPhysics(),
                      controller: _tabController,
                      children: const <Widget>[
                        _EventStack(),
                        _EventByCategory(category: 'wellbeing'),
                        _EventByCategory(category: 'growth'),
                        _EventByCategory(category: 'social'),
                      ],
                    ),
                  )
                ]);
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return LoaderProgresBar();
            })
      ],
    );
  }
}

/// EVENT STACK
/// ALL EVENTS
///
///
///
///
class _EventStack extends StatelessWidget {
  const _EventStack({super.key});

  @override
  Widget build(BuildContext context) {
    final eventsService = Provider.of<EventsService>(context);

    return FutureBuilder<List<ResultEvents>>(
        future: eventsService.loadEvents(),
        builder:
            (BuildContext context, AsyncSnapshot<List<ResultEvents>> snapshot) {
          if (snapshot.hasData) {
            return Expanded(
              child: Container(
                color: Colors.transparent,
                padding: const EdgeInsets.all(0),
                margin: const EdgeInsets.all(0),
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data!.length,
                    shrinkWrap: true,
                    itemExtent: 310,
                    itemBuilder: (context, index) {
                      final data = snapshot.data![index];

                      return Container(
                        color: Colors.transparent,
                        width: MediaQuery.of(context).size.width,
                        child: PageView.builder(
                            itemCount: snapshot.data!.length,
                            physics: const NeverScrollableScrollPhysics(),
                            controller: PageController(viewportFraction: 1),
                            itemBuilder: (BuildContext context, int index) =>
                                GestureDetector(
                                    onTap: () {
                                      print(snapshot.data![index].id);

                                      final dtos =
                                          eventsService.getDetailEvent(data.id);
                                      eventsService.selectedEvent = data;
                                      Navigator.pushNamed(
                                        context,
                                        'eventdetail',
                                      );
                                    },
                                    child: EventCard(data: data))),
                      );
                    }),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return LoaderProgresBar();
        });
  }
}

/// EVENT STACK
/// BY CATEGORY
///
///
///
///
class _EventByCategory extends StatelessWidget {
  const _EventByCategory({super.key, required this.category});
  final String category;

  @override
  Widget build(BuildContext context) {
    final eventsService = Provider.of<EventsService>(context);

    return FutureBuilder<List<ResultEvents>>(
        future: eventsService.loadEventsByCategory(category),
        builder:
            (BuildContext context, AsyncSnapshot<List<ResultEvents>> snapshot) {
          if (snapshot.data?.length == 0) {
            return Container(
                alignment: Alignment.topCenter,
                padding: const EdgeInsets.only(top: 50),
                child: const Text('No hay Eventos en esta categoría'));
          } else if (snapshot.hasData) {
            return Expanded(
              child: Container(
                color: Colors.transparent,
                padding: const EdgeInsets.all(0),
                margin: const EdgeInsets.all(0),
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data!.length,
                    shrinkWrap: true,
                    itemExtent: 310,
                    itemBuilder: (context, index) {
                      final data = snapshot.data![index];
                      return Container(
                        color: Colors.transparent,
                        width: MediaQuery.of(context).size.width,
                        child: PageView.builder(
                            itemCount: snapshot.data!.length,
                            physics: const NeverScrollableScrollPhysics(),
                            controller: PageController(viewportFraction: 1),
                            itemBuilder: (BuildContext context, int index) =>
                                GestureDetector(
                                    onTap: () async =>
                                        await Navigator.pushNamed(
                                            context, 'eventdetail'),
                                    child: EventCard(data: data))),
                      );
                    }),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return LoaderProgresBar();
        });
  }
}

///MENU MODEL
///
///
///
///
///
///
///
class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  bool get mostrar => this._mostrar;

  set mostrar(bool valor) {
    this._mostrar = valor;
    notifyListeners();
  }
}
