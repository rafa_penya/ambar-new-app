import 'package:ambar_abogados/src/pages/home_tab.dart';
import 'package:ambar_abogados/src/pages/lifestyle_tab.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/widgets/colors_constants.dart';
import 'package:ambar_abogados/utils/router_navigator.dart';
import 'package:flutter/material.dart';
import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:provider/provider.dart';

class TabsPage extends StatelessWidget {
  const TabsPage({super.key});

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return ChangeNotifierProvider(
      create: (final context) => NavigtionModel(),
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            toolbarHeight: 80,
            title: BrandSliver(),
            actions: [
              Builder(
                builder: (context) => Container(
                  margin: EdgeInsets.symmetric(horizontal: 0, vertical: 20),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context)
                          .push(createRoute(NotificationsScreen()));
                    },
                    child: Icon(
                        size: 18, Icons.notifications, color: Colors.black),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(), elevation: 0,
                      //  padding: EdgeInsets.all(3),
                      backgroundColor:
                          ColorConstants.inputBackground, // <-- Button color
                      foregroundColor: Colors.red, // <-- Splash color
                    ),
                  ),
                ),
              ),
              Builder(
                builder: (context) => Container(
                  margin: EdgeInsets.symmetric(horizontal: 0, vertical: 20),
                  child: ElevatedButton(
                    onPressed: () => _scaffoldKey.currentState?.openEndDrawer(),
                    child: Icon(size: 18, Icons.menu, color: Colors.black),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(), elevation: 0,
                      //padding: EdgeInsets.all(3),
                      backgroundColor:
                          ColorConstants.inputBackground, // <-- Button color
                      foregroundColor: Colors.red, // <-- Splash color
                    ),
                  ),
                ),
              ),
            ],
          ),
          extendBody: true,
          endDrawer: SideBarNavigation(),
          body: _Pages(),
          bottomNavigationBar:
              Stack(clipBehavior: Clip.none, children: [_Navigation()])),
    );
  }
}

///NAVIGATION
class _Navigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navModel = Provider.of<NavigtionModel>(context);

    return Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 0),
        child: FloatingNavbar(
          padding: EdgeInsets.only(left: 17, top: 10, right: 17, bottom: 10),
          itemBorderRadius: 0,
          margin: EdgeInsets.only(left: 17, right: 17),
          backgroundColor: Color.fromRGBO(247, 247, 247, 1),
          selectedBackgroundColor: Colors.transparent,
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.grey,
          onTap: (int val) => {navModel.currentPage = val},
          currentIndex: navModel.currentPage,
          items: [
            FloatingNavbarItem(icon: Icons.home_outlined, title: 'Home'),
            FloatingNavbarItem(
                icon: Icons.wb_sunny_outlined, title: 'Lifestyle'),
            FloatingNavbarItem(icon: Icons.bookmarks_sharp, title: 'Proyectos'),
            FloatingNavbarItem(
                icon: Icons.manage_accounts_outlined, title: 'Perfil'),
          ],
        ));
  }
}

///TAB PAGES
class _Pages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navController = Provider.of<NavigtionModel>(context);
    return PageView(
      controller: navController.pageController,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        HomeTab(),
        LifeStyle(),
        ProjectsTab(),
        ProfileTab(),
      ],
    );
  }
}

class NavigtionModel with ChangeNotifier {
  int _currentPage = 0;
  PageController _pageController = PageController();
  int get currentPage => this._currentPage;

  set currentPage(int value) {
    this._currentPage = value;
    _pageController.animateToPage(
      value,
      duration: Duration(milliseconds: 350),
      curve: Curves.easeIn,
    );
    notifyListeners();
  }

  PageController get pageController => this._pageController;
}
