import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/login_form_provider.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
            padding: EdgeInsets.all(20),
            child: Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 0,
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        child: SizedBox(
                          child: CloserButton(urlBack: 'login'),
                        )),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                        margin: EdgeInsets.only(top: 20, bottom: 5),
                        child: SizedBox(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: FadeInLeft(
                                  duration: Duration(milliseconds: 250),
                                  child: Text(
                                    'El lugar al que',
                                    style: TextStyles.h1Title
                                        .copyWith(color: Colors.black),
                                  ),
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: FadeInRight(
                                  duration: Duration(milliseconds: 250),
                                  child: Text(
                                    'perteneces',
                                    style: TextStyles.h1Title
                                        .copyWith(color: Colors.black),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )),
                  ),
                  Expanded(
                    flex: 6,
                    child: Container(
                        child: Container(
                      child: ChangeNotifierProvider(
                          create: (_) => LoginFormProvider(),
                          child: const _LoginForm()),
                    )),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      child: Container(
                        child: TextButton(
                          onPressed: () => Navigator.pushReplacementNamed(
                              context, 'register'),
                          child: Text(
                            '¿Aún no eres socio?',
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}

/// FORM NO EXPORT WIDGET
///
class _LoginForm extends StatelessWidget {
  const _LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);

    return Container(
        child: Form(
            key: loginForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 8),
                  alignment: Alignment.topLeft,
                  child: const Text(
                    "E-mail",
                    textAlign: TextAlign.left,
                  ),
                ),
                TextFormField(
                  autocorrect: false,
                  initialValue: 'rpenya@syntonize.com',
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                    hintText: 'john@email.com',
                    labelText: 'Correo electrónico',
                  ),
                  onChanged: (value) => loginForm.username = value,
                  validator: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    return regExp.hasMatch(value ?? '')
                        ? null
                        : 'El e-mail no tiene el formato adecuado';
                  },
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 8, top: 15),
                  alignment: Alignment.topLeft,
                  child: const Text(
                    "Contraseña",
                    textAlign: TextAlign.left,
                  ),
                ),
                TextFormField(
                  autocorrect: false,
                  initialValue: 'bSPmjjuCC*7W',
                  obscureText: true,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                    hintText: '***********',
                    labelText: 'Contraseña',
                  ),
                  onChanged: (value) => loginForm.password = value,
                  validator: (value) {
                    return (value != null && value.length >= 6)
                        ? null
                        : 'La contraseña debe de ser de 6 carácteres';
                  },
                ),
                Container(
                  padding: EdgeInsets.only(top: 15, bottom: 10),
                  alignment: Alignment.topLeft,
                  child: TextButton(
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, 'passrecovery'),
                    child: Text(
                      '¿Has olvidado tu contraseña?',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 30,
                    alignment: Alignment.bottomCenter,
                    child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        disabledColor: Colors.grey,
                        minWidth: 347,
                        elevation: 0,
                        color: ColorConstants.buttonsColor,
                        onPressed: loginForm.isLoading
                            ? null
                            : () async {
                                FocusScope.of(context).unfocus();
                                final authService = Provider.of<AuthService>(
                                    context,
                                    listen: false);
                                final storage = const FlutterSecureStorage();

                                if (!loginForm.isValidForm()) return;

                                loginForm.isLoading = true;

                                await authService.login(
                                    loginForm.username, loginForm.password);
                                final errorMessage =
                                    await storage.read(key: 'token');

                                if (errorMessage == null) {
                                  NotificationService.showSnackBar(
                                      'No se puede acceder');
                                  loginForm.isLoading = false;
                                } else {
                                  Navigator.pushReplacementNamed(
                                      context, 'home');
                                }

                                //    await Future.delayed((const Duration(seconds: 2)));
                              },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 80, vertical: 15),
                          child: loginForm.isLoading
                              ? Pulse(
                                  infinite: true,
                                  child: Text(
                                    "Espere...",
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                )
                              : Text(
                                  'Acceder',
                                  style: const TextStyle(color: Colors.white),
                                ),
                        )),
                  ),
                )
              ],
            )));
  }
}
