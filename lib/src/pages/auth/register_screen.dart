import 'package:flutter/material.dart';
import 'package:ambar_abogados/src/providers/login_form_provider.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/pages/ui/input_decorations.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AuthBackground(
            child: SingleChildScrollView(
      child: Column(children: [
        const SizedBox(
          height: 250,
        ),
        CardContainer(
            child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              'Login',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            const SizedBox(
              height: 30,
            ),
            ChangeNotifierProvider(
                create: (_) => LoginFormProvider(), child: const _LoginForm())
          ],
        )),
        const SizedBox(height: 50),
        TextButton(
          onPressed: () => Navigator.pushReplacementNamed(context, 'login'),
          child: Text(
            'Ingresar con tu cuenta',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        const SizedBox(height: 50),
      ]),
    )));
  }
}

class _LoginForm extends StatelessWidget {
  const _LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);

    return Container(
        child: Form(
            key: loginForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                TextFormField(
                  autocorrect: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                    hintText: 'john@email.com',
                    labelText: 'Correo electrónico',
                    prefixIcon: Icons.alternate_email_outlined,
                  ),
                  onChanged: (value) => loginForm.username = value,
                  validator: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    return regExp.hasMatch(value ?? '')
                        ? null
                        : 'El e-mail no tiene el formato adecuado';
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  autocorrect: false,
                  obscureText: true,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                      hintText: '***********',
                      labelText: 'Contraseña',
                      prefixIcon: Icons.lock_outline),
                  onChanged: (value) => loginForm.password = value,
                  validator: (value) {
                    return (value != null && value.length >= 6)
                        ? null
                        : 'La contraseña debe de ser de 6 carácteres';
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: Colors.grey,
                    elevation: 0,
                    color: Colors.deepPurple,
                    onPressed: loginForm.isLoading
                        ? null
                        : () async {
                            FocusScope.of(context).unfocus();

                            final authService = Provider.of<AuthService>(
                                context,
                                listen: false);

                            if (!loginForm.isValidForm()) return;
                            loginForm.isLoading = true;

                            final String? errorMessage =
                                await authService.createUser(
                                    loginForm.username, loginForm.password);

                            if (errorMessage == null) {
                              Navigator.pushReplacementNamed(context, 'home');
                            } else {
                              //TODO mostrar error en pantalla
                            }
                            //    await Future.delayed((const Duration(seconds: 2)));

                            loginForm.isLoading = false;
                          },
                    child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 80, vertical: 15),
                        child: Text(
                          loginForm.isLoading ? "Espere..." : 'Ingresar',
                          style: const TextStyle(color: Colors.white),
                        )))
              ],
            )));
  }
}
