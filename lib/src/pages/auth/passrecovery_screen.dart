import 'package:ambar_abogados/src/pages/ui/ui.dart';
import 'package:ambar_abogados/src/providers/login_form_provider.dart';
import 'package:flutter/material.dart';
import 'package:ambar_abogados/src/pages/pages.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/widgets/widgets.dart';
import 'package:provider/provider.dart';

class PassRecoveryScreen extends StatelessWidget {
  const PassRecoveryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
            padding: const EdgeInsets.all(20),
            child: Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 0,
                    child: Container(
                        margin: const EdgeInsets.only(top: 25),
                        child: const SizedBox(
                          child: BackerButton(urlBack: 'login'),
                        )),
                  ),
                  Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.only(top: 20, bottom: 8),
                          child: SizedBox(
                            child: Text(
                              '¿Has olvidado tu contraseña?',
                              style: TextStyles.h1Title
                                  .copyWith(color: Colors.black),
                            ),
                          )),
                      Container(
                        margin: const EdgeInsets.only(top: 15, bottom: 40),
                        child: const SizedBox(
                          child: Text(
                            'Introduce tu dirección de email para restablecer la contraseña.',
                          ),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    flex: 9,
                    child: Container(
                        child: Container(
                      child: ChangeNotifierProvider(
                          create: (_) => LoginFormProvider(),
                          child: const _LoginRecoveryForm()),
                    )),
                  ),
                ],
              ),
            )));
  }
}

/// FORM NO EXPORT WIDGET
///
class _LoginRecoveryForm extends StatelessWidget {
  const _LoginRecoveryForm({super.key});

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);

    return Container(
        child: Form(
            key: loginForm.formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  alignment: Alignment.topLeft,
                  child: const Text(
                    "E-mail",
                    textAlign: TextAlign.left,
                  ),
                ),
                TextFormField(
                  autocorrect: false,
                  initialValue: 'rpenya@syntonize.com',
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecorations.authInputDecoration(
                    hintText: 'john@email.com',
                    labelText: 'Correo electrónico',
                  ),
                  onChanged: (value) => loginForm.username = value,
                  validator: (value) {
                    String pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    return regExp.hasMatch(value ?? '')
                        ? null
                        : 'El e-mail no tiene el formato adecuado';
                  },
                ),
                Expanded(
                  child: Container(
                    height: 30,
                    alignment: Alignment.bottomCenter,
                    child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        disabledColor: Colors.grey,
                        minWidth: 347,
                        elevation: 0,
                        color: ColorConstants.buttonsColor,
                        onPressed: loginForm.isLoading
                            ? null
                            : () async {
                                FocusScope.of(context).unfocus();

                                final authService = Provider.of<AuthService>(
                                    context,
                                    listen: false);

                                if (!loginForm.isValidForm()) return;
                                loginForm.isLoading = true;

                                final String? errorMessage =
                                    await authService.login(
                                        loginForm.username, loginForm.password);

                                if (errorMessage == null) {
                                  Navigator.pushReplacementNamed(
                                      context, 'home');
                                } else {
                                  NotificationService.showSnackBar(
                                      errorMessage);
                                  loginForm.isLoading = false;
                                }
                                //    await Future.delayed((const Duration(seconds: 2)));
                              },
                        child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 80, vertical: 15),
                            child: Text(
                              loginForm.isLoading ? "Espere..." : 'Enviar',
                              style: const TextStyle(color: Colors.white),
                            ))),
                  ),
                ),
                const SizedBox(
                  height: 50,
                )
              ],
            )));
  }
}
