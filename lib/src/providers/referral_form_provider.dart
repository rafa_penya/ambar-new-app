import 'package:ambar_abogados/src/models/models.dart';
import 'package:flutter/material.dart';

class ReferralFormProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Referral referral;

  ReferralFormProvider(this.referral);

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }
}
