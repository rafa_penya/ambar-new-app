// ignore_for_file: unnecessary_this

import 'package:flutter/material.dart';
import 'package:ambar_abogados/src/models/models.dart';

class EventsFormProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  ResultEvents event;

  EventsFormProvider(this.event);

  updateAvailability(String value) {
    event.title = value;
    notifyListeners();
  }

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }
}
