import 'package:ambar_abogados/src/models/models.dart';
import 'package:flutter/material.dart';

class AwardsFormProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Awards awards;

  AwardsFormProvider(this.awards);

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }
}
