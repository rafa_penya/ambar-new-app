import 'package:ambar_abogados/src/models/profile.dart';
import 'package:flutter/material.dart';

class ProfileFormProvider extends ChangeNotifier {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Profile profile;

  ProfileFormProvider(this.profile);

  //Profile get selectedProfile => profile;

  bool isValidForm() {
    return formKey.currentState?.validate() ?? false;
  }
}
