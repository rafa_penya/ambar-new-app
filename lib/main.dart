import 'package:ambar_abogados/api/dio_api.dart';
import 'package:ambar_abogados/routes/routes.dart';
import 'package:ambar_abogados/src/services/referral_service.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:localization/localization.dart';
import 'package:ambar_abogados/src/pages/tabs_page.dart';
import 'package:ambar_abogados/src/services/services.dart';
import 'package:ambar_abogados/src/theme/ambar_theme.dart';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

void main() async {
  await dotenv.load(fileName: "lib/global/.env_dev");

  runApp(const AppState());
  DioApi.configureDio();
}

class AppState extends StatelessWidget {
  const AppState({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthService(),
        ),
        ChangeNotifierProvider(create: (_) => ProfileService()),
        ChangeNotifierProvider(create: (_) => EventsService()),
        ChangeNotifierProvider(create: (_) => ProdServService()),
        ChangeNotifierProvider(create: (_) => DiscountsService()),
        ChangeNotifierProvider(create: (_) => ReferralService()),
        ChangeNotifierProvider(create: (_) => AwardsService()),
      ],
      child: const MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

// Future<void> main() async {
//   DotEnv.load(fileName: '.env_dev');

//   runApp(const MyApp());
// }

  @override
  Widget build(BuildContext context) {
    LocalJsonLocalization.delegate.directories = ['lib/i18n'];

    return MaterialApp(
      home: const TabsPage(),
      theme: AmbarTheme,
      initialRoute: 'checking',
      routes: appRoutes,
      scaffoldMessengerKey: NotificationService.messengerKey,
      supportedLocales: const [
        Locale('en', 'US'),
        Locale('es', 'ES'),
      ],
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        LocalJsonLocalization.delegate,
      ],
    );
  }
}
